strough = social trough, a trough being where you put a feed (hah! like a social media feed)

strough is a messaging and social media client built from the ground up to be protocol-agnostic. It aims to fulfill a similar role to pidgin, where it handled common code and UI between various protocols and chat platforms, client side only.
That said, it aims to be even more customizable and hot-swappable, using AWT/Swing, and very modular, carefully crafted generic classes inspired by ActivityPub. You can swap out components based on platform, and the intent is to be able to use either of the two frontends (one inspired by tweetdeck, one inspired by mumble and discord), to mix and match to suit whatever your needs may be, be it a feed-based social media platform like rss or mastodon, or a chat based platform like IRC or xmpp.
This all said... it's very early days. The only protocol is a testing shell. I plan to implement IRC first as a proof of concept, but am working on the UI first and foremost, as that is the primary goal of the software: a modern chat and social media client UI, in Java.
Contributions: Very, very welcome. I am far from a high-skill or high-experience programmer, and I fully expect I will struggle at implementing protocols. Please feel welcome to contribute protocol code, or just help me make it better, whatever that means.

License: GPL

if you wish to contribute, your code would probably be best spent on a plugin loader or a protocol implementation
