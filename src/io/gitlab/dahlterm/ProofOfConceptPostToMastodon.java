package io.gitlab.dahlterm;

import com.grack.nanojson.JsonObject;
import com.grack.nanojson.JsonParser;

import javax.swing.*;
import java.awt.*;
import java.net.*;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class ProofOfConceptPostToMastodon {
private static HttpRequest.BodyPublisher buildFormDataFromMap(Map<Object, Object> data) {
	var builder = new StringBuilder();
	for (Map.Entry<Object, Object> entry : data.entrySet()) {
		if (builder.length() > 0) {
			builder.append("&");
		}
		builder.append(URLEncoder.encode(entry.getKey().toString(), StandardCharsets.UTF_8));
		builder.append("=");
		builder.append(URLEncoder.encode(entry.getValue().toString(), StandardCharsets.UTF_8));
	}
	System.out.println(builder.toString());
	return HttpRequest.BodyPublishers.ofString(builder.toString());
}
public static void main(String args[]) {
	try {
		///////////////////////////////////////
		/// OBTAIN APPLICATION ENTITY FIRST ///
		///////////////////////////////////////
		HttpClient httpClient = HttpClient
		    .newBuilder()
		    .version(
			  HttpClient.Version.HTTP_2
		    ).build();

		Map<Object, Object> data = new HashMap<>();
		data.put("client_name", "Strough");
		data.put("redirect_uris", "urn:ietf:wg:oauth:2.0:oob");
		data.put("scopes", "read write follow push");
		data.put("website", "https://gitlab.com/dialupdoll/strough");
		HttpRequest applicationRequest = HttpRequest
            .newBuilder().POST(
				buildFormDataFromMap(data)).uri(
			        URI.create("https://sleeping.town/api/v1/apps")
		        ).setHeader(
			  "Content-Type", "application/x-www-form-urlencoded"
		    ).build();

		HttpResponse<String> response = httpClient.send(applicationRequest, HttpResponse.BodyHandlers.ofString());

		// print status code
		System.out.println();
		System.out.println("application entity request:");
		System.out.println();
		System.out.println(response.statusCode());

		System.out.println();

		// print response body
		System.out.println(response.body());
		////////////////////////////////////
		/// OBTAIN AUTHENTICATION SECOND ///
		////////////////////////////////////

		JsonObject applicationEntity = JsonParser.object().from(response.body());
		data = new HashMap<>();
		data.put("client_id", applicationEntity.get("client_id"));
		data.put("client_secret", applicationEntity.get("client_secret"));
		data.put("redirect_uri", applicationEntity.get("redirect_uri"));
		data.put("grant_type", "client_credentials");

		HttpRequest tokenRequest = HttpRequest
		                        .newBuilder().POST(
			buildFormDataFromMap(data)).uri(
			URI.create("https://sleeping.town/oauth/token")
		  ).setHeader(
			"Content-Type", "application/x-www-form-urlencoded"
		  ).build();

		response = httpClient.send(tokenRequest, HttpResponse.BodyHandlers.ofString());

		// print status code
		System.out.println();
		System.out.println("app authentification:");
		System.out.println();
		System.out.println(response.statusCode());
		System.out.println();
		// print response body
		System.out.println(response.body());
		///////////////////////////////////
		/// FINALLY, VERIFY SUCCESSFULL ///
		///////////////////////////////////

		JsonObject applicationAccessToken =
		  JsonParser.object().from(response.body());

		data = new HashMap<>();
		data.put("access_token", applicationAccessToken.get("access_token"));


		HttpRequest verificationRequest = HttpRequest
		                             .newBuilder().POST(
			buildFormDataFromMap(data)).uri(
			URI.create("https://sleeping.town/api/v1/apps/verify_credentials")
		  ).setHeader(
			"Content-Type", "application/x-www-form-urlencoded"
		  ).build();

		response = httpClient.send(tokenRequest, HttpResponse.BodyHandlers.ofString());

		// print status code
		System.out.println();
		System.out.println("verification:");
		System.out.println();
		System.out.println(response.statusCode());

		System.out.println();

		// print response body
		System.out.println(response.body());

		System.out.println();

		////////////////////////////////////////////////////
		/// SEND AUTHORIZATION DISPLAY TO USER'S BROWSER ///
		////////////////////////////////////////////////////

		String urlAuthorizationOfUserAccount = constructAuthUrl("https://sleeping.town/oauth/authorize",
		  "code",
		  applicationEntity.get("client_id").toString(),
		  "urn:ietf:wg:oauth:2.0:oob",
		  "read write follow push",
		  true
		);
		System.out.println(urlAuthorizationOfUserAccount);

		Desktop.getDesktop().browse(new URI(urlAuthorizationOfUserAccount));
		String authorizationValidationCopyPaste = JOptionPane.showInputDialog(
		  "please paste in the code from oauth in-browser");

		///////////////////////////////////////////////////
		/// TURN IN AUTHORIZATION IN EXCHANGE FOR TOKEN ///
		///////////////////////////////////////////////////


		data = new HashMap<>();
		data.put("client_id", applicationEntity.get("client_id"));
		data.put("client_secret", applicationEntity.get("client_secret"));
		data.put("redirect_uri", applicationEntity.get("redirect_uri"));
		data.put("grant_type", "authorization_code");
		data.put("code", authorizationValidationCopyPaste);
		data.put("scope", "read write follow push");

		HttpRequest authorizationTokenRequest = HttpRequest
		                                    .newBuilder().POST(
			buildFormDataFromMap(data)).uri(
			URI.create("https://sleeping.town/oauth/token")
		  ).setHeader(
			"Content-Type", "application/x-www-form-urlencoded"
		  ).build();

		response = httpClient.send(authorizationTokenRequest, HttpResponse.BodyHandlers.ofString());

		System.out.println();
		System.out.println("authorization token handshake completion:");
		System.out.println();
		System.out.println(response.statusCode());
		System.out.println();

		System.out.println(response.body());

		JsonObject authorizationAccessToken =
		  JsonParser.object().from(response.body());

		///////////////////
		/// MAKE A POST ///
		///////////////////

		data = new HashMap<>();
		//data.put("authorization", authorization);
		data.put("status", "Hello from Strough via mastodon API");


		HttpRequest postStatusRequest = HttpRequest
		                                    .newBuilder().POST(
			buildFormDataFromMap(data)).uri(
			URI.create("https://sleeping.town/api/v1/statuses")
		  ).setHeader(
			"Content-Type", "application/x-www-form-urlencoded"
		  ).setHeader(
			"Authorization", "Bearer " + authorizationAccessToken.get(
			  "access_token")
		  ).build();

		response = httpClient.send(postStatusRequest, HttpResponse.BodyHandlers.ofString());


		System.out.println();
		System.out.println("post attempt:");
		System.out.println();
		System.out.println(response.statusCode());
		System.out.println();
		System.out.println(response.body());

	} catch (Exception e) {
		e.printStackTrace();
	}
}
	static String constructAuthUrl(String base_url,
	                      String response_type,
	                      String client_id,
	                      String redirect_uri, String scope,
	                      boolean force_login)
	{
		String url = base_url
		               + "?response_type="
		               + URLEncoder.encode(response_type, StandardCharsets.UTF_8)
		               + "&client_id="
		               + URLEncoder.encode(client_id, StandardCharsets.UTF_8)
		               + "&redirect_uri="
		               + URLEncoder.encode(redirect_uri, StandardCharsets.UTF_8)
		               + "&scope="
		               + URLEncoder.encode(scope, StandardCharsets.UTF_8)
		               + "&force_login="
		               + URLEncoder.encode(String.valueOf(force_login),
		  StandardCharsets.UTF_8);
		return url;
	}
}
/*
PINAFORE EXAMPLE URL: https://sleeping.town/oauth/authorize?client_id=PyBtyMXeXpfK-VceMHL3FoFXu4EqsxlTzcaybm_MVvw&redirect_uri=https%3A%2F%2Fpinafore.social%2Fsettings%2Finstances%2Fadd&response_type=code&scope=read%20write%20follow%20push

UNABRIDGED SOURCE CODE I LEARNED FROM
import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class Java11HttpClientExample {

    // one instance, reuse
    private final HttpClient httpClient = HttpClient.newBuilder()
            .version(HttpClient.Version.HTTP_2)
            .build();

    public static void main(String[] args) throws Exception {

        Java11HttpClientExample obj = new Java11HttpClientExample();

        System.out.println("Testing 1 - Send Http GET request");
        obj.sendGet();

        System.out.println("Testing 2 - Send Http POST request");
        obj.sendPost();

    }

    private void sendGet() throws Exception {

        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create("https://httpbin.org/get"))
                .setHeader("User-Agent", "Java 11 HttpClient Bot")
                .build();

        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        // print status code
        System.out.println(response.statusCode());

        // print response body
        System.out.println(response.body());

    }

    private void sendPost() throws Exception {

        // form parameters
        Map<Object, Object> data = new HashMap<>();
        data.put("username", "abc");
        data.put("password", "123");
        data.put("custom", "secret");
        data.put("ts", System.currentTimeMillis());

        HttpRequest request = HttpRequest.newBuilder()
                .POST(buildFormDataFromMap(data))
                .uri(URI.create("https://httpbin.org/post"))
                .setHeader("User-Agent", "Java 11 HttpClient Bot") // add request header
                .header("Content-Type", "application/x-www-form-urlencoded")
                .build();

        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        // print status code
        System.out.println(response.statusCode());

        // print response body
        System.out.println(response.body());

    }

    private static HttpRequest.BodyPublisher buildFormDataFromMap(Map<Object, Object> data) {
        var builder = new StringBuilder();
        for (Map.Entry<Object, Object> entry : data.entrySet()) {
            if (builder.length() > 0) {
                builder.append("&");
            }
            builder.append(URLEncoder.encode(entry.getKey().toString(), StandardCharsets.UTF_8));
            builder.append("=");
            builder.append(URLEncoder.encode(entry.getValue().toString(), StandardCharsets.UTF_8));
        }
        System.out.println(builder.toString());
        return HttpRequest.BodyPublishers.ofString(builder.toString());
    }

}

 */