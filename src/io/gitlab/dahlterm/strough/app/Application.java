package io.gitlab.dahlterm.strough.app;

import io.gitlab.dahlterm.strough.app.ui.Dashboard;
import io.gitlab.dahlterm.strough.app.ui.GenericWindow;

import javax.swing.*;

public class Application {
	
	private static void iterateAndOpen(LayoutNode node) {
		if(node == null) {
			return;
		}
		
		if(node.openOnLaunch) {
			node.createWindowAndShow();
		}
		
		for(LayoutNode iter : node.children()) {
			iterateAndOpen(iter);
		}
	}
	public static void main(String[] args) {
		Exception e = null;
		// this section will eventually set the look and feel requested by the user.
		try {
			// Set L&F
			//UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
		} catch (Exception ex) {
			// handle exception
			e = ex;
			try {
				UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
				e = null;
			} catch (Exception e2) {
				if (e != null)
					e2.printStackTrace();
			}
		} finally {
			if (e != null) {
				e.printStackTrace();
			}
		}

		
		Dashboard d = new Dashboard();
		
		GenericWindow example = new GenericWindow("Strough ALPHA Dashboard", d,
		  true);
		example.setVisible(true);

		PersistentData profile = PersistentData.getUserProfile();
		// here we will open all layouts the user requests to open-on-launch
		iterateAndOpen(profile.displayLayouts());
		
		

		// TODO: make the plugin loader actually function lmao. seems like will
		// probably need an entirely different approach to plugin loading.
		
		// System.out.println("Hello World from the SIO entrypoint!");
		//
		// PluginLoader<Protocol> loader = new PluginLoader<Protocol>();
		// try {
		// Protocol xmpp = loader.LoadClass("/home/dahlterm/Documents/dev/sio/",
		// "io.dahlterm.plugins.xmpp.ProtocolXMPP", Protocol.class);
		// System.out.println(xmpp.ProtocolName("en_us"));
		// } catch (Exception e) {
		// e.printStackTrace();
		// }
	}
}
