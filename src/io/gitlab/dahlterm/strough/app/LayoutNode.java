package io.gitlab.dahlterm.strough.app;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.UUID;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import com.grack.nanojson.JsonAppendableWriter;
import com.grack.nanojson.JsonArray;
import com.grack.nanojson.JsonObject;

import io.gitlab.dahlterm.strough.app.event.IEventReciever;
import io.gitlab.dahlterm.strough.app.protocol.IChannel;
import io.gitlab.dahlterm.strough.app.protocol.IIdentifier;
import io.gitlab.dahlterm.strough.app.ui.ChannelIdentifierWizard;
import io.gitlab.dahlterm.strough.app.ui.FeedView;
import io.gitlab.dahlterm.strough.app.ui.GenericWindow;

public abstract class LayoutNode {
	// logical groups consist of either a list of children logical groups, or an
	// inbox. they allow user based and automatic customization of organization

	String propertyNodeTitle = "";
	ArrayList<LayoutNode> propertyChildren = null;
IIdentifier.UUIDIdentifier id = null;
	
	public boolean openOnLaunch = false;
	public boolean isContractable = false;
	public String channelIdentifier = "";
	
	public void createWindowAndShow() {
		/// TODO: FIX GENERIC WINDOW FOR ARBITRARY LAYOUTS
		new GenericWindow(this.propertyNodeTitle, new FeedView(this), false).setVisible(true);
	}
	
	public final IIdentifier identity() {
		if(id == null) {
			id = IIdentifier.UUIDIdentifier.randomID();
		}
		return id;
	}
	
	public abstract IChannel endpoint();
	
	public static final String		keyNodeTitle = "node_title",
									keyUUID = "node_identifier",
									keyOpenOnLaunch = "open_on_launch",
									keyIsContractable = "is_contractable",
									keyChannelIdentifier = "channel_identifier",
									categoryConfiguredLayouts = "configured_layouts",
									keyChildrenList = "configured_children";
	
	private void writeToJSON(JsonAppendableWriter target, boolean isRoot) {
		if(isRoot) {
			target.object(categoryConfiguredLayouts);
		}

		target.value(keyNodeTitle, propertyNodeTitle);
		target.value(keyUUID, identity().toString());
		target.value(keyOpenOnLaunch, openOnLaunch);
		target.value(keyIsContractable, isContractable);
		target.value(keyChannelIdentifier, channelIdentifier);
		
		target.array(keyChildrenList);
		
			for(LayoutNode child : propertyChildren) {
				target.object();
					child.writeToJSON(target, false);
				target.end();
			}
		
		target.end();
		
		if(isRoot) {
			target.end();
		}
	}
	
	public void writeLayoutDataToJson(JsonAppendableWriter target) {
		writeToJSON(target, true);
		///TODO: error checking, maybe? idk.
	}
	
	private static LayoutNode readLayoutNodeFromJSON(JsonObject node, boolean isRoot) {
		LayoutNode result = null;
		if(isRoot) {
			result = createRoot();
		} else {
			result = createGeneric();
		}
		result.propertyNodeTitle = node.getString(keyNodeTitle);
		result.id = IIdentifier.UUIDIdentifier.fromStringOrUUID(node.getString(keyUUID));
		result.openOnLaunch = node.getBoolean(keyOpenOnLaunch);
		result.isContractable = node.getBoolean(keyIsContractable);
		result.channelIdentifier = node.getString(keyChannelIdentifier);
		
		JsonArray children = node.getArray(keyChildrenList);
		//children.iter
		result.propertyChildren = new ArrayList<LayoutNode>();
		if(children != null) {
			ListIterator<Object> iterator = children.listIterator();
			while(iterator.hasNext()) {
				JsonObject next_node = (JsonObject)iterator.next();
				LayoutNode child = readLayoutNodeFromJSON(next_node, false);
				result.propertyChildren.add(child);
			}
		}
			
		return result;
	}
	
	public static LayoutNode readFromJSON(JsonObject settings) {
		JsonObject rootObject = settings.getObject(categoryConfiguredLayouts);
		LayoutNode result = readLayoutNodeFromJSON(rootObject, true);
		///TODO: error checking goes here, maybe? idk.
		return result;
	}

	public ArrayList<LayoutNode> children() {
		if(propertyChildren == null)
			propertyChildren = new ArrayList<LayoutNode>();
		return propertyChildren;
	}
	
	public String nodeTitle() {
		return propertyNodeTitle;
	}
	
	
	public boolean containedWithin(LayoutNode search, ArrayList<LayoutNode> container) {
		for(LayoutNode node : container) {
			if(node.identity().equals(search.identity()))
				return true;
		}
		return false;
			
	}
	
	public LayoutNode searchChildrenForParent(LayoutNode search) {

		
		for(LayoutNode node : children()) {
			if(containedWithin(search, node.children())) {
				return node;
			} else {
				LayoutNode result = node.searchChildrenForParent(search);
				if(result != null)
					return result;
			}
		}
		
		return null;
	}

	public LayoutNode findParent(LayoutNode root) {
		if(containedWithin(this, root.children())) return root;
		return root.searchChildrenForParent(this);
	}
	
	public void addAbove(LayoutNode newNode, LayoutNode root) {

		LayoutNode parent = findParent(root);
		
		if(parent == null ) {
			root.children().add(newNode);
			System.out.println("parent wasn't found, for some reason.");
			return;
		}
		

		ArrayList<LayoutNode> edit = prelist(parent.children(), this);
		if(edit == null) {
			edit = new ArrayList<LayoutNode>();
		}
		edit.add(newNode);
		edit.add(this);
		edit.addAll(postlist(parent.children(), this));
		parent.propertyChildren = edit;
	}
	
	private ArrayList<LayoutNode> prelist(ArrayList<LayoutNode> group, LayoutNode target) {
		ArrayList<LayoutNode> result = new ArrayList<LayoutNode>();
		for(LayoutNode it : group) {
			if(it.equals(target)) {
				if(result.isEmpty())
					return null;
				else
					return result;
			}
			result.add(it);
		}
		//very weird, this should never occur.
		
		System.err.println("you are using this wrong. the prelist can only be obtained from a list that has the target in it");
		return null;
	}
	
	private ArrayList<LayoutNode> postlist(ArrayList<LayoutNode> group, LayoutNode target) {
		ArrayList<LayoutNode> result = new ArrayList<LayoutNode>();
		boolean hasBeenSeen = false;
		for(LayoutNode it : group) {
			if(hasBeenSeen) {
				result.add(it);
			} else {
				if(it.equals(target)) {
					hasBeenSeen = true;
				}
			}
		}
		if(!hasBeenSeen) {
			System.err.println("you are using this wrong. the postlist can only be obtained from a list that has the target in it");
			return null;
		}
		return result;
	}
	
	public void moveUp(LayoutNode root) {
		if(root == this)
			return;
		
		LayoutNode parent = findParent(root);
		
		ArrayList<LayoutNode> edit = prelist(parent.children(), this);
		
		//now that we have the list of elements before our target, fetch the previous element, which we must swap with, to move the element upward
		if(edit == null) {
			System.out.println("can't move upward!");
			return;
		}
		LayoutNode swap = edit.get(edit.size() - 1);
		edit.remove(swap);
		edit.add(this);
		edit.add(swap);
		
		edit.addAll(postlist(parent.children(), this));
		
		// this updates the list to reflect the new edit
		parent.propertyChildren = edit;
	}

	public void moveDown(LayoutNode root) {
		if(root == this)
			return;
		
		LayoutNode parent = findParent(root);
		
		ArrayList<LayoutNode> edit = prelist(parent.children(), this);
		ArrayList<LayoutNode> post = postlist(parent.children(), this);

		if(post == null) {
			System.out.println("can't move downward!");
			return;
		}
		if(post.size() == 0) {
			System.out.println("can't move downward!");
			return;
		}
		if(edit == null) {
			edit = new ArrayList<LayoutNode>();
		}
		//the first element of the post list is our swapped element
		LayoutNode swap = post.get(0);
		post.remove(swap);
		edit.add(swap);
		edit.add(this);
		edit.addAll(post);
		
		// this updates the list to reflect the new edit
		parent.propertyChildren = edit;
	}
	
	public boolean hasAfter(LayoutNode root) {
		if(root == this)
			return false;
		
		LayoutNode parent = findParent(root);

		ArrayList<LayoutNode> post = postlist(parent.children(), this);
		
		if(post.size() == 0) {
			return false;
		}
		
		return true;
	}
	
	public boolean hasBefore(LayoutNode root) {

		if(root == this)
			return false;
		
		LayoutNode parent = findParent(root);
		
		ArrayList<LayoutNode> edit = prelist(parent.children(), this);
		
		if(edit == null) {
			return false;
		}
		
		return true;
	}

	public void shrink(LayoutNode root) {
		if(root == this)
			return;

		LayoutNode parent = findParent(root);
		
		LayoutNode previous = null;
		for(LayoutNode it : parent.children()) {
			if(it.equals(this)) {
				break;	// the previous object is now the thing we plan to put the object into
			}
			previous = it;
		}
		
		if(previous != null) {
			if(previous.parentalType() != 1 & previous.parentalType() != 4 & previous.parentalType() != 5 & previous.parentalType() != 6) {	
				return;
				// duck out early, we cannot put it into a node that is not a container
			}
			//remove from current parent
			parent.children().remove(this);
			previous.children().add(this);
		}
		
	}
	
	public void expand(LayoutNode root) {
		if(root == this)
			return;
		LayoutNode parent = findParent(root);

		LayoutNode newParent = parent.findParent(root);

		ArrayList<LayoutNode> edit = prelist(newParent.children(), parent);
		if(edit == null) {
			// there are no previous elements, so we make a new empty list
			edit = new ArrayList<LayoutNode>();
		}
		edit.add(parent);
		edit.add(this);
		edit.addAll(postlist(newParent.children(), parent));
		parent.children().remove(this);
		newParent.propertyChildren = edit;
	}

	public abstract byte parentalType();
	
	public Component nodeEditor(IEventReciever updatable) {
		final IEventReciever updated = updatable;

		final LayoutNode thisNode = this;
		JPanel result = new JPanel();
		result.setLayout(new BoxLayout(result, BoxLayout.Y_AXIS));
		
		JPanel temp;

		result.add(new JPanel());
		
		temp = new JPanel();
		temp.add(new JPanel());
			temp.setLayout(new BoxLayout(temp, BoxLayout.X_AXIS));
			temp.add(new JLabel("Channel Title / Nickname: "));
			
			JTextField jtf_title = new JTextField(propertyNodeTitle);
			
			final JTextField textFieldTitle = jtf_title;
			jtf_title.addFocusListener(new FocusListener() {
				
				public void focusLost(FocusEvent e) {
					propertyNodeTitle = textFieldTitle.getText();
					updated.receiveEvent(this);
					
				}
				
				public void focusGained(FocusEvent e) {
					// TODO Auto-generated method stub
					
				}
			});
			temp.add(jtf_title);
			temp.add(new JPanel());
		result.add(temp);
		
		result.add(new JPanel());
		
		temp = new JPanel();

			temp.add(new JPanel());
			temp.setLayout(new BoxLayout(temp, BoxLayout.X_AXIS));

			JCheckBox jcb_isContractable = new JCheckBox("Contractable", isContractable);
			
			final JCheckBox isContractableChecked = jcb_isContractable;
			jcb_isContractable.addActionListener(new ActionListener() {
				
				public void actionPerformed(ActionEvent e) {
					isContractable = isContractableChecked.isSelected();
					updated.receiveEvent(null);
				}
			});
			temp.add(jcb_isContractable);
			temp.add(new JPanel());
			
		result.add(temp);
		
		temp = new JPanel();

			temp.add(new JPanel());
			temp.setLayout(new BoxLayout(temp, BoxLayout.X_AXIS));

			JCheckBox jcb_openOnLaunch = new JCheckBox("Open On Launch", openOnLaunch);
			
			final JCheckBox checkOpenOnLaunch = jcb_openOnLaunch;
			checkOpenOnLaunch.addActionListener(new ActionListener() {
				
				public void actionPerformed(ActionEvent e) {
					openOnLaunch = checkOpenOnLaunch.isSelected();
					updated.receiveEvent(null);
				}
			});
			temp.add(jcb_openOnLaunch);
			temp.add(new JPanel());
			
		result.add(temp);
		
		temp = new JPanel();

			temp.add(new JPanel());
			temp.setLayout(new BoxLayout(temp, BoxLayout.X_AXIS));

			JButton openButton = new JButton("Open");
			
			final JButton jb_openButton = openButton;
			openButton.addActionListener(new ActionListener() {
				
				public void actionPerformed(ActionEvent e) {
					createWindowAndShow();
				}
			});
			temp.add(jb_openButton);
			temp.add(new JPanel());
			
		result.add(temp);

		result.add(new JPanel());
		
		temp = new JPanel();
			temp.add(new JPanel());
			temp.setLayout(new BoxLayout(temp, BoxLayout.X_AXIS));
			temp.add(new JLabel("Channel Identifier: "));
			
			JTextField jtf_channelIdentifier = new JTextField(channelIdentifier);
			
			final JTextField textFieldID = jtf_channelIdentifier;
			jtf_channelIdentifier.getDocument().addDocumentListener(new DocumentListener() {
				
				public void removeUpdate(DocumentEvent e) {
					actionPerformed();
				}
				
				public void insertUpdate(DocumentEvent e) {
					actionPerformed();
				}
				
				public void changedUpdate(DocumentEvent e) {
					actionPerformed();
				}
				public void actionPerformed() {
					channelIdentifier = textFieldID.getText();
					
				}
			});
			temp.add(jtf_channelIdentifier);
			JButton openWiz = new JButton("Wizard");
			openWiz.addActionListener(new ActionListener() {
				
				public void actionPerformed(ActionEvent e) {
					ChannelIdentifierWizard.show(thisNode, updated);
				}
			});
			temp.add(openWiz);
			temp.add(new JPanel());
			
		result.add(temp);
		
		// VALIDATE CHANNEL IDENTIFIER
		boolean hasChildNodeChannel = checkForOpenableContent(thisNode);
		if(hasChildNodeChannel) {
			// ^ if true, can be set to open on launch or opened with button press
			jb_openButton.setEnabled(true);
			checkOpenOnLaunch.setEnabled(true);
		} else {
			jb_openButton.setEnabled(false);
			checkOpenOnLaunch.setEnabled(false);
			checkOpenOnLaunch.setSelected(false);
		}
		
		
		return result;
	}
	
	public boolean checkForOpenableContent(LayoutNode focus) {
		boolean isOpenable = false;
		final PersistentData profile = PersistentData.getUserProfile();
		for(LayoutNode node : focus.children()) {
			IChannel c = profile.endpointFromSearchString(node.channelIdentifier, focus);
			System.out.println(node.nodeTitle() + " " + node.channelIdentifier);
			
			if(c != null) {
				return true;
			}
			isOpenable = checkForOpenableContent(node);
		}
		return isOpenable;
	}
	
	public static final byte 
		ROOT = 0,
		
			// these nodes are purely visual, like folders in discord
		BLANK = 1, PLACEHOLDER = 1, VISUAL = 1, GROUPING = 1,
		
			// endpoints, or non-parents, are the places that we actually expose channels
		ENDPOINT = 2,
		
			//predefined nodes cannot be edited in the layout editor, that may or may not have children. in this way, they act as wildcards, so you could add a layout node that consists of all the direct messages available to you on a service, or just your notifications column, and users have a predefined way to add it to their layouts.
		PREDEFINED = 3,	
				
			// the following are to implement later - they will act as an alternate way to display the nodes that are it's children (think tabbed as twitter style tabs to switch between, columned as tweetdeck or mastodon style columns. these also function as a middleground between predefined nodes and generic nodes - they can have custom children, and thus can be "edited" in a way that predefined nodes cannot
		TABBED = 4,
		COLUMNED = 5,
		CUSTOM_PARENT = 6;
	
	public static LayoutNode createRoot() {
		LayoutNode result = new LayoutNode() {
			
			@Override
			public byte parentalType() {
				return ROOT;
			}
			
			@Override
			public IChannel endpoint() {
				return null;
			}
		};
		result.propertyChildren = new ArrayList<LayoutNode>();
		return result;
	}
	
	public static LayoutNode createGeneric() {
		return new LayoutNode() {
			
			@Override
			public byte parentalType() {
				return 1;
			}
			
			@Override
			public IChannel endpoint() {
				PersistentData profile = PersistentData.getUserProfile();
				IChannel c = profile.endpointFromSearchString(this.channelIdentifier, this);
				
				return c;
			}
		};
	}
}
