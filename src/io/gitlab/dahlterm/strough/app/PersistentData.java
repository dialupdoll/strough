package io.gitlab.dahlterm.strough.app;

import com.grack.nanojson.JsonAppendableWriter;
import com.grack.nanojson.JsonArray;
import com.grack.nanojson.JsonObject;
import io.gitlab.dahlterm.strough.app.event.IAccountRegisteredEventReciever;
import io.gitlab.dahlterm.strough.app.event.IEventReciever;
import io.gitlab.dahlterm.strough.app.protocol.IAccount;
import io.gitlab.dahlterm.strough.app.protocol.IChannel;
import io.gitlab.dahlterm.strough.app.protocol.IProtocol;
import io.gitlab.dahlterm.strough.app.mastodonapi.MastodonProtocol;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.UUID;

public class PersistentData implements IAccountRegisteredEventReciever, IEventReciever {
	
	/**
	 * yep, this is a singleton: if you want multiple user profiles, you can open
	 * a second instance of the program. This is primarily to keep things as easy
	 * for end users to understand as possible: it would be relatively simple to
	 * support two profiles at once, however, it would make it even more easy for
	 * users to get lost and confused in the various accounts and protocols.
	 */
	private static PersistentData singletonUserProfile = null;

	
	
	/**
	 * This boolean determines where saved data is stored about accounts and such.
	 * True means it will be stored directly beside the jar file. Ideally this
	 * would be stored somewhere and loaded into memory, but where would it be
	 * stored, if not directly beside the jar file? I'm unsure if this could break
	 * compatibility with some operating systems and install methods, so for now
	 * it's hardcoded.
	 */
	public final boolean portableApplicationMode = true;
	private LayoutNode propertyLayouts = LayoutNode.createRoot();
	private ArrayList<IProtocol> propertyProtocols = null;
	private ArrayList<IAccount> propertyAccounts = null;
	private boolean propertySendRequiresControl = false;
	private String propertyProfileName = null;
	/// Here, we pass the layoutnode in so that they can update the id for their channel / endpoint, should it change.
	public IChannel endpointFromSearchString(String identifier, LayoutNode node) {
		if(identifier.split(":").length < 3)
			return null;
		IChannel result = null;
		String s_protocol = identifier.split(":")[0];
		String s_account = identifier.split(":")[1];
		String s_endpoint = identifier.split(":")[2];
		IProtocol p_protocol = bySearchString(s_protocol, node);
		if(p_protocol != null) {
			IAccount acc = p_protocol.bySearchString(s_account, node);
			if(acc != null) {
				result = acc.bySearchString(s_endpoint, node);
			}
		}
		return result;
	}
	
	/// Here, we pass the layoutnode in so that they can update the id for their channel / endpoint, should it change.
	private IProtocol bySearchString(String protocolName, LayoutNode node) {
		IProtocol result = null;
		System.out.println(propertyProtocols.size());
		for(IProtocol prot : propertyProtocols) {
			if(prot.nickname().equals(protocolName)) {
				if(result == null) {
					result = prot;
				} else {
					JOptionPane.showMessageDialog(null, "Multiple protocols exist with this nickname! This probably means you registered one protocol twice, somehow.", "Duplicate Protocol Nicknames!", JOptionPane.ERROR_MESSAGE);
					return null;
				}
			} else {
				try {
					if(prot.identifier().equals(UUID.fromString(protocolName))) {
						return prot;
					}
				} catch (IllegalArgumentException ile) {
					/// this is expected! do nothing. this just means we didn't pass in a UUID format, because it wasn't one.
				}
			}
		}
		return result;
	}
	
	private void propertyChanged() {
		//sio's profile / properties system saves after every change.
		//this localizes the auto-save system so we can make it manual
		//and to make it easy to have a central place to handle it all,
		//especially for error handling.
		
		saveProfile();
	}
	
	public void forgetAccount(IAccount acc) {
		propertyAccounts.remove(acc);
		propertyChanged();
	}
	
	public ArrayList<IAccount> getRegisteredAccounts() {
		return propertyAccounts;
	}
	
	public void accountRegistered(IAccount acc) {
		propertyAccounts.add(acc);
		propertyChanged();
	}
	
	public ArrayList<IProtocol> getRegisteredProtocols() {
		return propertyProtocols;
	}
	
	public void saveLayouts() {
		propertyChanged();
	}
	
	public LayoutNode displayLayouts() {
		return propertyLayouts;	
	}
	
	private PersistentData() {
		propertyAccounts = new ArrayList<IAccount>();
		propertyProtocols = new ArrayList<IProtocol>();
		propertyLayouts = LayoutNode.createRoot();
		loadProtocols();
	}
	
	public boolean getSendRequiresControl() {
		return propertySendRequiresControl;
	}
	
	public void setSendRequiresControl(boolean mode) {
		propertySendRequiresControl = mode;
		propertyChanged();
	}
	
	public static PersistentData getUserProfile() {
		if (singletonUserProfile != null) {
			return singletonUserProfile;
		} else {
			singletonUserProfile = loadProfile(defaultProfileName());
			if(singletonUserProfile == null) {
				return createDefaultProfile();
			} else {
				return singletonUserProfile;
			}
		}
	}
	
	private static PersistentData createDefaultProfile() {
		singletonUserProfile = new PersistentData();
		singletonUserProfile.saveProfile();
		return singletonUserProfile;
	}

	
	public void remove(IAccount acc) {
		
	}
	
	public void remove(IProtocol protocol) {
		
	}

	
	private static final String keyPropertySendRequiresControl = "send_behaviour_require_control";



	public static final String keyAccountNickname = "account_nickname";



	public static final String keyAccountUUID = "account_identifier";



	public static final String keyAccountProtocol = "account_protocol";



	public static final String keyAccountProtocolUUID = "account_protocol_identifier";



	private static final String categoryGeneralSettings = "general_settings";



	private static final String categoryRegisteredAccounts = "registered_accounts";



	private static final String fileExtension = ".sio.json";
								
	public void saveAccount(IAccount acc) {

			String accountPath = dataFolderProfilePath() + acc.nickname() +
			                       " - " + acc.identifier().toString()  + File.separator;
			System.out.println(accountPath);
			System.out.println(acc.identifier().toString());
			verifyExists(accountPath);
			
			
			acc.owningProtocol().saveToFile(acc, accountPath);
		
	}
	
	public void saveProfile() {
		try {
			FileOutputStream fileWriter = new FileOutputStream(dataFolderProfilePath() + getProfileName() + fileExtension, false);
			JsonAppendableWriter jsonWriter =
			  Utilities.openFileToWrite(fileWriter);
			
			//					beginning of json writer
			jsonWriter.object();
			//					beginning of json writer
			
				//				general / app settings
				jsonWriter.object(categoryGeneralSettings);
					jsonWriter.value(keyPropertySendRequiresControl, propertySendRequiresControl);
					//jsonWriter.value(keyTotalNumberOfRegisteredAccounts, propertyAccounts.size());
				jsonWriter.end();
				//				end of general/app settings
				
				//				protocols
				for(IProtocol prot : propertyProtocols) {
					
				}
				//				end of protocols
				
				propertyLayouts.writeLayoutDataToJson(jsonWriter);

				//				start of account saving
				jsonWriter.array(categoryRegisteredAccounts);
				//				start of account saving
					for(int i = 0; i < propertyAccounts.size(); i++) {
						String accountNickname = propertyAccounts.get(i).nickname();
						String accountIdentifier = propertyAccounts.get(i).identifier().toString();
						String accountProtocol = propertyAccounts.get(i).owningProtocol().nickname();
						String accountProtocolIdentifier = propertyAccounts.get(i).owningProtocol().identifier().toString();
						
						jsonWriter.object();
							jsonWriter.value(keyAccountUUID, accountIdentifier);
							jsonWriter.value(keyAccountNickname, accountNickname);
							jsonWriter.value(keyAccountProtocol, accountProtocol);
							jsonWriter.value(keyAccountProtocolUUID, accountProtocolIdentifier);
						jsonWriter.end();
						
						saveAccount(propertyAccounts.get(i));
						
					}
				//				end of account saving
				jsonWriter.end();
				//				end of account saving
			
			
			//					end of json file
			jsonWriter.end();
			//					end of json file
			
			jsonWriter.done();
			
			fileWriter.flush();
			fileWriter.close();
			System.out.println("profiled saved to files successfully.");
			
		} catch (FileNotFoundException e) {
			System.err.println("File Not Found Exception thrown");
			e.printStackTrace();
			return;
		} catch (IOException e) {
			System.err.println("IO Exception thrown");
			e.printStackTrace();
			return;
		} catch (NumberFormatException e) {
			System.err.println("Number format Exception thrown, unable to read the total number of accounts or layouts from file.");
			e.printStackTrace();
			return;
		}
	}
	
	public IAccount loadAccount(String nickname, UUID id, IProtocol prot) {

		String accountPath =
		  dataFolderProfilePath() + nickname + " - " + id + File.separator;
		verifyExists(accountPath);

		IAccount acc = prot.loadRegisteredAccountFromDisk(accountPath);
		return acc;
	}

	
	public static PersistentData loadProfile(String nameOfProfileToLoad) {
		
		try {
			PersistentData result = new PersistentData();

			result.propertyProfileName = nameOfProfileToLoad;
			
			//todo: allow for multiple profiles via profile selector on launch, then use that data here.
			
			JsonObject settings =
			  Utilities.openFileToRead(result.dataFolderProfilePath() + result.getProfileName() + fileExtension);
			if(settings == null) {
				return result;
			}
			
				JsonObject general = settings.getObject(categoryGeneralSettings);
					result.propertySendRequiresControl = general.getBoolean(keyPropertySendRequiresControl);
		
				result.propertyLayouts = LayoutNode.readFromJSON(settings);
					
				JsonArray accounts = settings.getArray(categoryRegisteredAccounts);
				
					ListIterator<Object> iterator = accounts.listIterator();

					//		iterate through all accounts registered
					while(iterator.hasNext()) {
						JsonObject account = (JsonObject) iterator.next();
						UUID protocolID = UUID.fromString(account.getString(keyAccountProtocolUUID));
						UUID accountID = UUID.fromString(account.getString(keyAccountUUID));
						String nickname = account.getString(keyAccountNickname);
						IProtocol protocol = result.findProtocol(protocolID);
						if(result.propertyAccounts == null) {
							result.propertyAccounts = new ArrayList<IAccount>();
						}
						if(protocol == null) {
							System.err.println("Can't find protocol");
							continue;
						}
						IAccount acc = result.loadAccount(nickname, accountID, protocol);
						if(acc != null) {
							result.propertyAccounts.add(acc);
						}
					}
			
			singletonUserProfile = result;
			System.out.println("profiled loaded from files successfully.");
			return singletonUserProfile;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public IProtocol findProtocol(UUID identifier) {
		for(IProtocol prot : propertyProtocols) {
			if(prot.identifier().equals(identifier))
				return prot;
		}
		return null;
	}
	
	public static String defaultProfileName() {
		//this will allow for loading a properties file that specifies what the default profile is
		return "default";
	}
	
	public String getLocalization() {
		return "en_us";
	}
	
	private boolean verifyExists(String path) {
		File f = new File(path);
		if (!f.exists()) {
			boolean response = f.mkdirs();
			if(!response) {
				System.out.println("not all directories made successfully!");
			} else {

				System.out.println("directories made");
			}
		}
		return true;
	}
	
	/**
	 * currently only supports portable mode
	 * @return The path to the folder selected to work within for data saving.
	 */
	public String dataFolderProfilePath() {
		String path = Thread.currentThread().getContextClassLoader().getResource(".").getPath() + File.separator + getProfileName() + " profile" + File.separator;
		verifyExists(path);
		return path;
	}
	
	public String dataFolderAccountPath(IAccount account) {
		String profile = dataFolderProfilePath();
		String accountFolder = profile + File.separator + account.nickname() + " - " + account.hashCode();
		verifyExists(accountFolder);
		return accountFolder;
	}
	
	public static JPanel editUserProfile() {
		final PersistentData profile = getUserProfile();
		JPanel profilePreferencesEditor = new JPanel();
		profilePreferencesEditor.setLayout(new BoxLayout(profilePreferencesEditor, BoxLayout.Y_AXIS));
		profilePreferencesEditor.setBorder(new EmptyBorder(10, 10, 10, 10));
		
		JPanel reusedVariable = null; //reused once per setting
		
		reusedVariable = new JPanel();
		reusedVariable.setLayout(new BoxLayout(reusedVariable, BoxLayout.X_AXIS));
		reusedVariable.add(new JPanel());
		ButtonGroup composeBoxSendBehaviour = new ButtonGroup();
		JToggleButton controlSend = new JToggleButton("Control+Enter to send message, Enter for newline");
		controlSend.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				profile.setSendRequiresControl(true);
			}
			
		});
		composeBoxSendBehaviour.add(controlSend);
		reusedVariable.add(controlSend);
		reusedVariable.add(new JPanel());

		JToggleButton simplySend = new JToggleButton("Enter to send, Shift+Enter for newline");
		simplySend.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				profile.setSendRequiresControl(false);
			}
			
		});
		composeBoxSendBehaviour.add(simplySend);
		reusedVariable.add(simplySend);
		reusedVariable.add(new JPanel());
		if(!profile.propertySendRequiresControl) simplySend.setSelected(true);
		if(profile.propertySendRequiresControl) controlSend.setSelected(true);
		
		profilePreferencesEditor.add(reusedVariable);	
		

		reusedVariable = new JPanel();
		
		reusedVariable.setBorder(new EmptyBorder(10, 10, 10, 10));
		
		reusedVariable.add(new JLabel("S-IO makes use of Nanojson without changes to Nanojson's code. Nanojson is licensed under the Apache License."));

		profilePreferencesEditor.add(reusedVariable);	
		
		return profilePreferencesEditor;
	}
	
	public String getProfileName() {
		if(propertyProfileName == null) {
			propertyProfileName = "default";
			propertyChanged();
		}
		return propertyProfileName;
	}
	
	public void setProfileName(String newName) {
		propertyProfileName = newName;
		//TODO: delete old properties file? for now we only support one properties file so its nbd.
		propertyChanged();
	}
	
	private void loadProtocols() {
		if(!propertyProtocols.isEmpty()) {
			propertyProtocols.clear();
		}
		propertyProtocols.add(new MastodonProtocol());
		System.out.println(propertyProtocols.toString());
	}

	public void receiveEvent(Object event) {
		propertyChanged();
	}
	
	public static ArrayList<IAccount> accountsBy(IProtocol filterParentProtocol) {
		PersistentData userRegistry = PersistentData.getUserProfile();
		if(filterParentProtocol == null) {
			return userRegistry.getRegisteredAccounts();
		}
		ArrayList<IAccount> result = new ArrayList<IAccount>();
		for(IAccount iterator : userRegistry.getRegisteredAccounts()) {
			if(iterator.owningProtocol().equals(filterParentProtocol)) {
				result.add(iterator);
			}
		}
		return result;
	}
	public static ArrayList<IChannel> channelsBy(IAccount filterOwningAccount) {
		PersistentData userRegistry = PersistentData.getUserProfile();
		ArrayList<IChannel> result = new ArrayList<IChannel>();
		if(filterOwningAccount == null ) {
			for(IAccount accountIterator : userRegistry.getRegisteredAccounts()) {
				for(IChannel channelIterator : accountIterator.availableChannels()) {
					result.add(channelIterator);
				}
			}
		} else {
			for(IChannel iterator : filterOwningAccount.availableChannels()) {
				result.add(iterator);
			}
		}
		return result;
	}
}
