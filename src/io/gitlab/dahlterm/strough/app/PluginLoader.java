package io.gitlab.dahlterm.strough.app;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

/// found this code online, haven't gotten it to work. keeping it in for legacy/later attempts to make it work, if nobody pitches in a pluginloader.
public class PluginLoader<C> {
	public C loadClass(String directory, String classpath, Class<C> parentClass) throws ClassNotFoundException {
	    File pluginsDir = new File(directory);
	    for (File jar : pluginsDir.listFiles()) {
	      try {
	        ClassLoader loader = URLClassLoader.newInstance(
	            new URL[] { jar.toURI().toURL() },
	            getClass().getClassLoader()
	        );
	        Class<?> clazz = Class.forName(classpath, true, loader);
	        Class<? extends C> newClass = clazz.asSubclass(parentClass);
	        // Apparently its bad to use Class.newInstance, so we use 
	        // newClass.getConstructor() instead
	        Constructor<? extends C> constructor = newClass.getConstructor();
	        return constructor.newInstance();
	        
	      } catch (ClassNotFoundException e) {
	        // There might be multiple JARs in the directory,
	        // so keep looking
	        continue;
	      } catch (MalformedURLException e) {
	        e.printStackTrace();
	      } catch (NoSuchMethodException e) {
	        e.printStackTrace();
	      } catch (InvocationTargetException e) {
	        e.printStackTrace();
	      } catch (IllegalAccessException e) {
	        e.printStackTrace();
	      } catch (InstantiationException e) {
	        e.printStackTrace();
	      }
	    }
	    throw new ClassNotFoundException("Class " + classpath
	        + " wasn't found in directory " + System.getProperty("user.dir") + directory);
	  }
}
