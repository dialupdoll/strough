package io.gitlab.dahlterm.strough.app;

import java.awt.Image;
import java.net.URL;

import javax.imageio.ImageIO;

import io.gitlab.dahlterm.strough.app.Application;

public class Resources {
	public static Image loadResource(String resourceIdentifier) {
		URL url = Application.class.getResource(resourceIdentifier);
		if(url == null) return null;
		try {
			return ImageIO.read(url);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
