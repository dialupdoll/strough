package io.gitlab.dahlterm.strough.app;

import com.grack.nanojson.*;

import java.awt.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.net.http.HttpRequest;
import java.nio.charset.StandardCharsets;
import java.util.Map;

public class Utilities {

public static JsonAppendableWriter openFileToWrite(FileOutputStream file) {
	try {
		return JsonWriter.indent("\t").on(file);
	} catch(Exception e) {
		e.printStackTrace();
	}
	return null;
}

public static void redraw(Component c) {
	c.repaint();
	c.revalidate();
}

public static JsonObject openFileToRead(String file) {
	try {
		FileInputStream fis = new FileInputStream(file);
		JsonObject result =  JsonParser.object().from(fis);
		if(result==null) {
			System.out.println("huh?");
		}
		fis.close();
		return result;

	} catch (FileNotFoundException e) {
		System.err.println("File Not Found Exception thrown");
		e.printStackTrace();
	} catch (NumberFormatException e) {
		System.err.println("Number format Exception thrown, unable to read the total number of accounts or layouts from file.");
		e.printStackTrace();
	} catch (JsonParserException e) {
		System.err.println("Json Parser Exception thrown.");
		e.printStackTrace();
	} catch (IOException e) {
		System.err.println("IO Exception thrown.");
		e.printStackTrace();
	}

	return null;

}
public static HttpRequest.BodyPublisher buildFormDataFromMap(Map<Object, Object> data) {
	var builder = new StringBuilder();
	for (Map.Entry<Object, Object> entry : data.entrySet()) {
		if (builder.length() > 0) {
			builder.append("&");
		}
		builder.append(URLEncoder.encode(entry.getKey().toString(), StandardCharsets.UTF_8));
		builder.append("=");
		builder.append(URLEncoder.encode(entry.getValue().toString(), StandardCharsets.UTF_8));
	}
	System.out.println(builder.toString());
	return HttpRequest.BodyPublishers.ofString(builder.toString());
}

public static String constructAuthUrl(String base_url,
                               String response_type,
                               String client_id,
                               String redirect_uri, String scope,
                               boolean force_login)
{
	String url = base_url
	               + "?response_type="
	               + URLEncoder.encode(response_type, StandardCharsets.UTF_8)
	               + "&client_id="
	               + URLEncoder.encode(client_id, StandardCharsets.UTF_8)
	               + "&redirect_uri="
	               + URLEncoder.encode(redirect_uri, StandardCharsets.UTF_8)
	               + "&scope="
	               + URLEncoder.encode(scope, StandardCharsets.UTF_8)
	               + "&force_login="
	               + URLEncoder.encode(String.valueOf(force_login),
	  StandardCharsets.UTF_8);
	return url;
}
}
