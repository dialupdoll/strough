package io.gitlab.dahlterm.strough.app.event;

import io.gitlab.dahlterm.strough.app.protocol.IAccount;

public interface IAccountRegisteredEventReciever {
	public void accountRegistered(IAccount acc);
}
