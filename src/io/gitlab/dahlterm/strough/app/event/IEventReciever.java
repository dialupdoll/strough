package io.gitlab.dahlterm.strough.app.event;

public interface IEventReciever {
	public void receiveEvent(Object event);
}
