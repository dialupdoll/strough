package io.gitlab.dahlterm.strough.app.mastodonapi;

import com.grack.nanojson.*;
import io.gitlab.dahlterm.strough.app.Utilities;
import io.gitlab.dahlterm.strough.app.protocol.IAction;
import io.gitlab.dahlterm.strough.app.LayoutNode;
import io.gitlab.dahlterm.strough.app.protocol.IObjectCapabilities;
import io.gitlab.dahlterm.strough.app.event.IEventReciever;
import io.gitlab.dahlterm.strough.app.protocol.*;

import javax.swing.*;
import java.awt.*;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.*;

public class MastodonAccount implements IAccount {

String clientID = null;
public static final String CLIENT_ID_KEY = "client_id";

static final String REDIRECT_URI = "urn:ietf:wg:oauth:2.0:oob";

String accessToken = null;
static final String ACCESS_TOKEN_KEY = "access_token";

String clientSecret = null;
public static final String CLIENT_SECRET_KEY = "client_secret";

String accountUserName = null;
public static final String ACCOUNT_HANDLE = "account_handle";

String apiURL = null;
public static final String API_URL_KEY = "instance_api_url";

final MastodonAccount self = this;

public void saveToFile(String file) {
	file += "account.json";
	try {
		FileOutputStream fos = new FileOutputStream(file, false);
		JsonAppendableWriter jaw = Utilities.openFileToWrite(fos);
		jaw.object();
		jaw.value(CLIENT_SECRET_KEY, clientSecret);
		jaw.value(CLIENT_ID_KEY, clientID);
		jaw.value(ACCOUNT_HANDLE, accountUserName);
		jaw.value(ACCESS_TOKEN_KEY, accessToken);
		jaw.value(API_URL_KEY, apiURL);
		jaw.end();
		jaw.done();
		fos.flush();
		fos.close();
	} catch (Exception e) {
		e.printStackTrace();
	}
}

public boolean initializeFromFile(String file) {
	file += "account.json";
	System.out.println(file);
	try {
		JsonObject account = Utilities.openFileToRead(file);
		clientSecret = account.getString(CLIENT_SECRET_KEY);
		clientID = account.getString(CLIENT_ID_KEY);
		accountUserName = account.getString(ACCOUNT_HANDLE);
		accessToken = account.getString(ACCESS_TOKEN_KEY);
		apiURL = account.getString(API_URL_KEY);
		uuid = IIdentifier.UUIDIdentifier.randomID();
		return true;
	} catch (Exception e) {
		e.printStackTrace();
		return false;
	}
}

private boolean initialized() {
	if (uuid == null) {
		return false;
	}
	return true;
}

public boolean registerApplication(String url) {
	try {
		apiURL = url;
		///////////////////////////////////////
		/// OBTAIN APPLICATION ENTITY FIRST ///
		///////////////////////////////////////
		HttpClient httpClient = HttpClient
		                          .newBuilder()
		                          .version(
			                        HttpClient.Version.HTTP_2
		                          ).build();

		Map<Object, Object> data = new HashMap<>();
		data.put("client_name", "Strough");
		data.put("redirect_uris", REDIRECT_URI);
		data.put("scopes", "read write follow push");
		data.put("website", "https://gitlab.com/dialupdoll/strough");
		HttpRequest applicationRequest = HttpRequest
		                                   .newBuilder().POST(
			Utilities.buildFormDataFromMap(data)).uri(
			URI.create(apiURL + "api/v1/apps")
		  ).setHeader(
			"Content-Type", "application/x-www-form-urlencoded"
		  ).build();

		HttpResponse<String> response = httpClient.send(applicationRequest, HttpResponse.BodyHandlers.ofString());

		// print status code
		System.out.println();
		System.out.println("application entity request:");
		System.out.println();
		System.out.println(response.statusCode());

		System.out.println();

		// print response body
		System.out.println(response.body());
		////////////////////////////////////
		/// OBTAIN AUTHENTICATION SECOND ///
		////////////////////////////////////

		JsonObject applicationEntity = JsonParser.object().from(response.body());
		data = new HashMap<>();
		data.put("client_id", applicationEntity.get("client_id"));
		data.put("client_secret", applicationEntity.get("client_secret"));
		data.put("redirect_uri", REDIRECT_URI);
		data.put("grant_type", "client_credentials");
		data.put("scopes", "read write follow push");
		this.clientID = applicationEntity.get("client_id").toString();
		this.clientSecret = applicationEntity.get("client_secret").toString();

		HttpRequest tokenRequest = HttpRequest
		                             .newBuilder().POST(
			Utilities.buildFormDataFromMap(data)).uri(
			URI.create(apiURL + "oauth/token")
		  ).setHeader(
			"Content-Type", "application/x-www-form-urlencoded"
		  ).build();

		response = httpClient.send(tokenRequest, HttpResponse.BodyHandlers.ofString());

		// print status code
		System.out.println();
		System.out.println("app authentification:");
		System.out.println();
		System.out.println(response.statusCode());
		System.out.println();
		// print response body
		System.out.println(response.body());
		///////////////////////////////////
		/// FINALLY, VERIFY SUCCESSFULL ///
		///////////////////////////////////

		JsonObject applicationAccessToken =
		  JsonParser.object().from(response.body());

		data = new HashMap<>();
		data.put("access_token", applicationAccessToken.get("access_token"));
		accessToken = applicationAccessToken.get("access_token").toString();


		HttpRequest verificationRequest = HttpRequest
		                                    .newBuilder().POST(
			Utilities.buildFormDataFromMap(data)).uri(
			URI.create(apiURL + "api/v1/apps/verify_credentials")
		  ).setHeader(
			"Content-Type", "application/x-www-form-urlencoded"
		  ).build();

		response = httpClient.send(tokenRequest, HttpResponse.BodyHandlers.ofString());

		// print status code
		System.out.println();
		System.out.println("verification:");
		System.out.println();
		System.out.println(response.statusCode());

		System.out.println();

		// print response body
		System.out.println(response.body());

		System.out.println();

	} catch (Exception e) {
		e.printStackTrace();
	}
	uuid = IIdentifier.UUIDIdentifier.randomID();
	return true;
}

public boolean registerAccount() {
	try {
		////////////////////////////////////////////////////
		/// SEND AUTHORIZATION DISPLAY TO USER'S BROWSER ///
		////////////////////////////////////////////////////

		String urlAuthorizationOfUserAccount = Utilities.constructAuthUrl(
		  apiURL + "oauth/authorize",
		  "code",
		  this.clientID,
		  "urn:ietf:wg:oauth:2.0:oob",
		  "read write follow push",
		  true
		);
		System.out.println(urlAuthorizationOfUserAccount);

		Desktop.getDesktop().browse(new URI(urlAuthorizationOfUserAccount));
		String authorizationValidationCopyPaste = JOptionPane.showInputDialog(
		  "please paste in the code from oauth in-browser");

		///////////////////////////////////////////////////
		/// TURN IN AUTHORIZATION IN EXCHANGE FOR TOKEN ///
		///////////////////////////////////////////////////


		Map<Object, Object> data = new HashMap<>();
		data.put("client_id", this.clientID);
		data.put("client_secret", this.clientSecret);
		data.put("redirect_uri", this.REDIRECT_URI);
		data.put("grant_type", "authorization_code");
		data.put("code", authorizationValidationCopyPaste);
		data.put("scope", "read write follow push");


		HttpRequest authorizationTokenRequest = HttpRequest
		                                          .newBuilder().POST(
			Utilities.buildFormDataFromMap(data)).uri(
			URI.create(apiURL + "oauth/token")
		  ).setHeader(
			"Content-Type", "application/x-www-form-urlencoded"
		  ).build();

		HttpClient httpClient = HttpClient
		                          .newBuilder()
		                          .version(
			                        HttpClient.Version.HTTP_2
		                          ).build();
		HttpResponse<String> response = httpClient.send(authorizationTokenRequest
		  , HttpResponse.BodyHandlers.ofString());

		System.out.println();
		System.out.println("authorization token handshake completion:");
		System.out.println();
		System.out.println(response.statusCode());
		if (response.statusCode() != 200) {
			return false;
		}

		System.out.println();

		System.out.println(response.body());

		JsonObject authorizationAccessToken =
		  JsonParser.object().from(response.body());
		accessToken = authorizationAccessToken.get(
		  "access_token").toString();


		HttpRequest userCredentialsRequest = HttpRequest
		                                       .newBuilder().GET().uri(
			URI.create(apiURL + "api/v1/accounts/verify_credentials")
		  ).setHeader(
			"Content-Type", "application/x-www-form-urlencoded"
		  ).setHeader(
			"Authorization", "Bearer " + accessToken
		  ).build();

		response = httpClient.send(userCredentialsRequest
		  , HttpResponse.BodyHandlers.ofString());

		JsonObject userCredentials =
		  JsonParser.object().from(response.body());

		System.out.println();
		System.out.println("account info:");
		System.out.println();
		System.out.println(response.statusCode());
		if (response.statusCode() != 200) {
			return false;
		}
		accountUserName = userCredentials.get("acct").toString();

		System.out.println();

		System.out.println(response.body());

		return true;

	} catch (JsonParserException e) {
		throw new RuntimeException(e);
	} catch (IOException e) {
		throw new RuntimeException(e);
	} catch (URISyntaxException e) {
		throw new RuntimeException(e);
	} catch (InterruptedException e) {
		throw new RuntimeException(e);
	}
}

public MastodonAccount() {
}

IIdentifier.UUIDIdentifier uuid = null;

IProtocol parent = null;

String fullyQualifiedUserName = null;

@Override
public IAction[] properties() {
	if (!initialized())
		return null;
	return new IAction[0];
}

private IChannel HomeTimeline = new IChannel() {
	IAccount acc = self;
	IIdentifier.UUIDIdentifier identifier = IIdentifier.UUIDIdentifier.fromStringOrUUID("b937e5f8-6077-4122-8e75-01360a9f7414");

	@Override
	public IContentPage retrievePage(IContentPage previous) {
		return new MastodonHomeTimelinePage();
	}

	class MastodonHomeTimelinePage implements IContentPage {
	String results = "a";
		ArrayList<MastodonMessage> pageContents = new ArrayList<>();

		public MastodonHomeTimelinePage() {
			try {

				Map<Object, Object> data = new HashMap<>();
				data = new HashMap<>();
				data.put("limit", 20);


				HttpRequest postStatusRequest = HttpRequest
				                                  .newBuilder().uri(
					URI.create(apiURL + "api/v1/timelines/home")
				  ).GET().setHeader(
					"Content-Type", "application/x-www-form-urlencoded"
				  ).setHeader(
					"Authorization", "Bearer " + accessToken
				  ).build();
				System.out.println("Using access token: " + accessToken);

				HttpClient httpClient = HttpClient
				                          .newBuilder()
				                          .version(
					                        HttpClient.Version.HTTP_2
				                          ).build();
				HttpResponse response = httpClient.send(postStatusRequest,
				  HttpResponse.BodyHandlers.ofString());
				results =  response.body().toString();
				System.out.println(results);
				JsonArray array = JsonParser.array().from(results);

				//pageContents.add(new MastodonMessage(results));
				for(Object iterator : array) {
						pageContents.add(new MastodonMessage((JsonObject) iterator));
				}
				Collections.reverse(pageContents);

			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Using access token: " + apiURL);
			}

		}

		@Override
		public IMessage[] contents() {
			return pageContents.toArray(new IMessage[0]);
		}

		@Override
		public int notificationBadge() {
			return 0;
		}
	}

	@Override
	public String title() {
		return "Home Timeline";
	}

	@Override
	public String topic() {
		return "The place that holds the people you follow.";
	}

	@Override
	public String channelIdentifier() {
		return "htl";
	}

	@Override
	public IIdentifier identifier() {
		return identifier;
	}

	@Override
	public void setUpdateReciever(IEventReciever recipient) {

	}

	@Override
	public void send(IMessage m) {
		try {
			Map<Object, Object> data = new HashMap<>();
			data = new HashMap<>();
			//data.put("authorization", authorization);
			data.put("status", m.asText());


			HttpRequest postStatusRequest = HttpRequest
			                                  .newBuilder().POST(
				Utilities.buildFormDataFromMap(data)).uri(
				URI.create(apiURL + "api/v1/statuses")
			  ).setHeader(
				"Content-Type", "application/x-www-form-urlencoded"
			  ).setHeader(
				"Authorization", "Bearer " + accessToken
			  ).build();
			System.out.println("Using access token: " + accessToken);

			HttpClient httpClient = HttpClient
			                          .newBuilder()
			                          .version(
				                        HttpClient.Version.HTTP_2
			                          ).build();
			HttpResponse response = httpClient.send(postStatusRequest,
			  HttpResponse.BodyHandlers.ofString());


			System.out.println();
			System.out.println("post attempt:");
			System.out.println();
			System.out.println(response.statusCode());
			System.out.println();
			System.out.println(response.body());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public IMessage createMessageFromText(String text) {
		return new IMessage() {
			@Override
			public String asText() {
				return text;
			}

			@Override
			public Component customComponent() {
				return null;
			}

			@Override
			public LayoutNode container() {
				return null;
			}

			@Override
			public void setUpdateReciever(IEventReciever er) {

			}

			@Override
			public IAction[] properties() {
				return new IAction[0];
			}
		};
	}

	@Override
	public void requestDeletion(IMessage m) {

	}

	@Override
	public IAccount owningAccount() {
		return acc;
	}

	@Override
	public IAction[] properties() {
		return new IAction[]{
		  new IAction() {
			  @Override
			  public String actionTitle() {
				  return "null";
			  }

			  @Override
			  public void performAction(IObjectCapabilities target) {

			  }

			  @Override
			  public String helpInformation() {
				  return "null";
			  }

			  @Override
			  public IIdentifier identifier() {
				  return null;
			  }
		  }
		};
	}
};

@Override
public IChannel[] availableChannels() {
	return new IChannel[]{
	  HomeTimeline
	};
}

@Override
public String nickname() {
	if (!initialized())
		return null;
	if (fullyQualifiedUserName != null)
		return fullyQualifiedUserName;
	else
		return accountUserName;
}

@Override
public IIdentifier identifier() {
	if (!initialized())
		return null;
	return uuid;
}

@Override
public ArrayList<LayoutNode> defaultHierarchys() {
	if (!initialized())
		return null;
	return null;
}

@Override
public Component accountSettingsConfigurationMenu(IEventReciever uer) {
	return new Panel();
}

@Override
public IProtocol owningProtocol() {
	return MastodonProtocol.protocol;
}

@Override
public IChannel bySearchString(String nick, LayoutNode node) {
	if (!initialized())
		return null;
	return HomeTimeline;
}
}
