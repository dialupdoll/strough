package io.gitlab.dahlterm.strough.app.mastodonapi;

import com.grack.nanojson.JsonObject;
import io.gitlab.dahlterm.strough.app.protocol.IAction;
import io.gitlab.dahlterm.strough.app.LayoutNode;
import io.gitlab.dahlterm.strough.app.protocol.IIdentifier;
import io.gitlab.dahlterm.strough.app.protocol.IObjectCapabilities;
import io.gitlab.dahlterm.strough.app.event.IEventReciever;
import io.gitlab.dahlterm.strough.app.protocol.IMessage;

import java.awt.*;
import java.util.UUID;

public class MastodonMessage implements IMessage {
JsonObject json = null;
String asText = null;

private void parseFormatting() {
	String result = json.getString("content");
	result = removeFirst_From_(result, "<p>");
	result = removeLast_From_(result, "</p>");
	result = removeFirst_From_(result, "<span>");
	result = removeLast_From_(result, "</span>");
	result = replaceAll_From_With_(result, "<p></p>", System.lineSeparator());
	asText = result;
}

String replaceAll_From_With_(String subject, String target,
                             String replacement) {
	String result = subject.replace(target, replacement);
	return result;
}

String removeLast_From_(String subject, String substring) {
	String replaced = invert(subject).replaceFirst(invert(substring), "");
	return invert(replaced);
}

String invert(String subject) {
	StringBuilder builder = new StringBuilder();
	builder.append(subject);
	builder.reverse();
	return builder.toString();
}

String removeFirst_From_(String subject, String substring) {
	String replaced = subject.replaceFirst(substring, "");
	return replaced;
}

public MastodonMessage(JsonObject json) {
	try {
		this.json = json;
		parseFormatting();

	} catch(Exception e) {
		e.printStackTrace();
	}
}

@Override
public IAction[] properties() {
	return new IAction[] {
	  new IAction() {

		  @Override
		  public String actionTitle() {
			  return "a";
		  }

		  @Override
		  public void performAction(IObjectCapabilities target) {

		  }

		  @Override
		  public String helpInformation() {
			  return "a";
		  }

		  @Override
		  public IIdentifier identifier() {
			  return null;
		  }
	  }
	};
}

@Override
public String asText() {
	if(asText != null) {
return asText;
	}
	return "error";
}

@Override
public Component customComponent() {
	return null;
}

@Override
public LayoutNode container() {
	return null;
}

@Override
public void setUpdateReciever(IEventReciever er) {

}
}
