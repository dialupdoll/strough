package io.gitlab.dahlterm.strough.app.mastodonapi;

import io.gitlab.dahlterm.strough.app.LayoutNode;
import io.gitlab.dahlterm.strough.app.event.IAccountRegisteredEventReciever;
import io.gitlab.dahlterm.strough.app.protocol.IAccount;
import io.gitlab.dahlterm.strough.app.protocol.IIdentifier;
import io.gitlab.dahlterm.strough.app.protocol.IProtocol;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.UUID;

public class MastodonProtocol implements IProtocol {

public static final MastodonProtocol protocol = new MastodonProtocol();

static final ArrayList<IAccount> accounts = new ArrayList<>();

static final IIdentifier.UUIDIdentifier uuid = IIdentifier.UUIDIdentifier.fromStringOrUUID("7f0ac3cb-169a-4fa2-a0ba-ce2538eaaf79");
@Override
public IIdentifier identifier() {
	return uuid;
}
@Override
public void authorizeNewAccount(IAccountRegisteredEventReciever arer) {
	String instanceURL = JOptionPane.showInputDialog(
	  "please enter the full URL for your instance (including https:// and a ), example: \"https://sleeping.town/\"");

	MastodonAccount account = new MastodonAccount();
	System.out.println(instanceURL);
	account.registerApplication(instanceURL);
	if(account.registerAccount()) {
		System.out.println("Successful");
		accounts.add(account);
		arer.accountRegistered(account);
	}
}

@Override
public String nickname() {
	return "MastodonAPI";
}

@Override
public String protocolShortDescription() {
	return "Mastodon API client protocol for fediverse microblogging ActivityPub technologies";
}

@Override
public Component protocolSettingsConfigurationMenu() {
	return new Panel() {
		{
			add(new Label("Not implemented, yet"));
		}
	};
}

@Override
public IAccount loadRegisteredAccountFromDisk(String folder) {

	MastodonAccount result = new MastodonAccount();
	if(result.initializeFromFile(folder)) {
		accounts.add(result);
		return result;
	}
	return null;
}

@Override
public void saveToFile(IAccount acc, String folder) {
	if(acc instanceof MastodonAccount macc) {
		macc.saveToFile(folder);
	} else {
		System.err.println("Couldn't save to file! - not a mastodon account we recognize");
	}
}

@Override
public IAccount bySearchString(String nick, LayoutNode node) {

	for(IAccount iterator : accounts) {
		return iterator;
	}

	return null;
}
}
