
package io.gitlab.dahlterm.strough.app.plain_text;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;

import io.gitlab.dahlterm.strough.app.protocol.IChannel;
import io.gitlab.dahlterm.strough.app.PersistentData;

public class DefaultComposeComponent extends Panel {

	JButton sendMessage = new JButton(">");
	JTextArea messageText = new JTextArea();
	IChannel target = null;

	public void sendMessage() {
		if (messageText.getText().trim().equals(""))
			return;

		target.send(target.createMessageFromText(messageText.getText()));
		messageText.setText("");
	}

	public DefaultComposeComponent(IChannel chat) {
		target = chat;
		
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		
		messageText.setLineWrap(true);
		messageText.setMinimumSize(new Dimension(100, 20));
		messageText.setAlignmentX(Component.LEFT_ALIGNMENT);
		messageText.setBorder(new EmptyBorder(5, 5, 5, 5));

		//below: this extra panel is needed to have a beveled border (for looks) 
		//in addition to an indented text-draw-area (for reader's legibility)
		JPanel borderedPanel = new JPanel();
		borderedPanel.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
		borderedPanel.setLayout(new BorderLayout());
		borderedPanel.add(messageText, BorderLayout.CENTER);
		this.add(borderedPanel);
		

		sendMessage.setAlignmentX(Component.RIGHT_ALIGNMENT);
		this.add(sendMessage);
		
		sendMessage.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				sendMessage();
			}
			
		});

		messageText.addKeyListener(new KeyListener() {
			// todo: allow ctrl+enter to be the way to send, instead of just normal enter, and shift enter dodging it
			boolean shiftPressed = false, controlPressed = false;
			public void keyTyped(KeyEvent e) {
				// doesn't matter to us at all. we aren't dealing in typable characters really, here
			}

			public void keyPressed(KeyEvent e) {
				PersistentData pd = PersistentData.getUserProfile();
				// this just stores data about our two relevant modifier keys
				if (e.getKeyCode() == KeyEvent.VK_SHIFT) {
					shiftPressed = true;
				}
				if(e.getKeyCode() == KeyEvent.VK_CONTROL) {
					controlPressed = true;
				}
				// here's where we actually send messages with keybinds
				if(pd.getSendRequiresControl()) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						if (controlPressed) {
							sendMessage();
							e.consume();
						}
						// no else block needed - ctrl enter behaviour by default uses natural newline addition in any old textbox
					}
				} else {
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						if (!shiftPressed) {
							sendMessage();
							e.consume();
						} else if(shiftPressed) {
							messageText.setText(messageText.getText() + System.lineSeparator());
						}
					}
				}
			}

			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_SHIFT) {
					shiftPressed = false;
				}
				if(e.getKeyCode() == KeyEvent.VK_CONTROL) {
					controlPressed = false;
				}

			}

		});

	}

}
