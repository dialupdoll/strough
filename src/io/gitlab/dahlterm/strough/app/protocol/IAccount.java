package io.gitlab.dahlterm.strough.app.protocol;

import java.awt.Component;
import java.util.ArrayList;
import java.util.UUID;

import io.gitlab.dahlterm.strough.app.LayoutNode;
import io.gitlab.dahlterm.strough.app.event.IEventReciever;

//Account extends ObjectCapabilities to allow protocol-specific, per-account preferences, and settings. this can be client-specific, such as disabling the favorite button, or something relevant to the protocol or account in question, like changing your password or changing your profile picture
public interface IAccount extends IObjectCapabilities {
	public IChannel[] availableChannels();
	public String nickname();
	public IIdentifier identifier();
	// below: preconfigured slot-in layoutnodes (with children, sometimes) that act as common functions in an account, like a list of all accounts you have friended, or all the people you follow
	public ArrayList<LayoutNode> defaultHierarchys();
	//TODO: allow for accounts to have images / profile pictures
	
	public Component accountSettingsConfigurationMenu(IEventReciever uer);
	public IProtocol owningProtocol();
	
	public IChannel bySearchString(String nick, LayoutNode node);
}
