package io.gitlab.dahlterm.strough.app.protocol;

import java.util.UUID;

//an object capability action
public interface IAction {
	public String actionTitle();
	public void performAction(IObjectCapabilities target);
	public String helpInformation();
	public IIdentifier identifier();
}
