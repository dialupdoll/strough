package io.gitlab.dahlterm.strough.app.protocol;

import java.util.UUID;

import io.gitlab.dahlterm.strough.app.event.IEventReciever;

//an channel is any location that a user can receive messages to be displayed and read. for instance, your home timeline, your public timeline, local timeline, your notifications, your direct messages
public interface IChannel extends IObjectCapabilities {
	
	//the pagination system is to allow for posts to be loaded in reasonable chunks decided appropriate to the protocol in question. to accomplish this, pages start from null, instead of using a numbering system. to retrieve the latest and most up to date, or otherwise default page, you pass null into RetrievePage
	public IContentPage retrievePage(IContentPage previous);
	
	//this is the title of the inbox as represented to the user. this can be as simple as the name of the contact, the name of the timeline, or anything else. format is up to the protocol implementation itself, but the title will be displayed to the user alongside the account that it is associated with
	public String title();
	public String topic();
	public String channelIdentifier();
	public IIdentifier identifier();
	
	public void setUpdateReciever(IEventReciever recipient);
	
	public void send(IMessage m);
	public IMessage createMessageFromText(String text);
	public void requestDeletion(IMessage m);
	
	public IAccount owningAccount();
}
