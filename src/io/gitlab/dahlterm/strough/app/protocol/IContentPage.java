package io.gitlab.dahlterm.strough.app.protocol;

public interface IContentPage {
	
	public IMessage[] contents();
	//the notification badge property is used to determine and display the number of notifications in a group, for display numerically by count, if the column is configured to do so
	public int notificationBadge();
}
