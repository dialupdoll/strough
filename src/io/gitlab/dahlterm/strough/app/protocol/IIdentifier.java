package io.gitlab.dahlterm.strough.app.protocol;

import java.util.UUID;

public interface IIdentifier {
	public Boolean equalTo(IIdentifier other);
	public class UUIDIdentifier implements IIdentifier {
		UUID uid = null;

		public static UUIDIdentifier randomID() {
			UUIDIdentifier result = new UUIDIdentifier();
			result.uid = UUID.randomUUID();
			return result;
		}

		public static UUIDIdentifier fromStringOrUUID(Object o) {
			UUIDIdentifier result = new UUIDIdentifier();
			if(o instanceof String) {
				String s = (String)o;
				result.uid = UUID.fromString(s);
				return result;
			}
			if(o instanceof UUID) {
				UUID uuid = (UUID) o;
				result.uid = uuid;
				return result;
			}
			return null;

		}


		@Override
		public Boolean equalTo(IIdentifier other) {
			if (uid == null) {
				return null;
			}
			if(other instanceof UUIDIdentifier) {
				UUIDIdentifier uuidIdentifier = (UUIDIdentifier) other;

				if(uuidIdentifier.uid == null) {
					return null;
				}

				if(uuidIdentifier.uid.compareTo(uid) == 0) {
					return true;
				} else {
					return false;
				}
			}

			return null;
		}
	}
}
