package io.gitlab.dahlterm.strough.app.protocol;

import java.awt.Component;

import io.gitlab.dahlterm.strough.app.LayoutNode;
import io.gitlab.dahlterm.strough.app.event.IEventReciever;

public interface IMessage extends IObjectCapabilities {
	public String asText();
	public Component customComponent();
	public LayoutNode container();
	public void setUpdateReciever(IEventReciever er);
}
