package io.gitlab.dahlterm.strough.app.protocol;

//the object capabilities system allows for per-protocol actions. this can mean favorites on posts, replies and threaded chats such as the fediverse or php, etc
public interface IObjectCapabilities {
	public IAction[] properties();
}
