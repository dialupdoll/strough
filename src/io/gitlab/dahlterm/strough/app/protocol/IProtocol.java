package io.gitlab.dahlterm.strough.app.protocol;

import java.awt.Component;
import java.util.UUID;

import io.gitlab.dahlterm.strough.app.LayoutNode;
import io.gitlab.dahlterm.strough.app.event.IAccountRegisteredEventReciever;

public interface IProtocol {
	//this will handle loading a dialog or other methods necessary to register an account with the application
	public void authorizeNewAccount(IAccountRegisteredEventReciever arer);
	
	public IIdentifier identifier();
	public String nickname();
	
	public String protocolShortDescription();

	public Component protocolSettingsConfigurationMenu();
	
	public IAccount loadRegisteredAccountFromDisk(String folder);
	
	public void saveToFile(IAccount Acc, String folder);
	
	public IAccount bySearchString(String nick, LayoutNode node);
}
