package io.gitlab.dahlterm.strough.app.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.UUID;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JToggleButton;
import javax.swing.border.EmptyBorder;

import io.gitlab.dahlterm.strough.app.*;
import io.gitlab.dahlterm.strough.app.protocol.*;
import io.gitlab.dahlterm.strough.app.event.IAccountRegisteredEventReciever;
import io.gitlab.dahlterm.strough.app.event.IEventReciever;

/*
 * Dashboard is a view that contains access points for all the currently connected protocols.
 * This includes access points for account settings, for adding and removing accounts, and
 * so on and so forth. Also contains settings for the user interface in general.
 */
public class Dashboard extends JPanel implements IAccountRegisteredEventReciever, IEventReciever {

	JSplitPane accountView = null;

	JTextArea noAccount = new JTextArea(
			"<html>Account-specific settings and preferences will appear here when an account is selected.</html>");

	JTextArea noProtocol = new JTextArea(
			"<html>Protocol-specific settings and preferences will appear here when an account is selected.</html>");

	JTabbedPane tabHandler;

	Dashboard totalContainer = null;

	IAccount selected = null;

	JSplitPane protocolView = null;

	public Dashboard() {
		super();

		tabHandler = new JTabbedPane();

		/*
		 * set up the layout view editor. this lets users design their interface/channel
		 * list to fit their needs
		 */

		createLayoutView();

		/*
		 * next, set up the account view. if we're serving a new user, we will want to
		 * switch to this as a new user will want to set up an account, first, before
		 * anything else. TODO: detect new users and force swap to this tab.
		 */

		createAccountView();

		/*
		 * sets up the protocols view. This is where users can edit protocol specific
		 * settings, like how often you update all of a protocol's accounts by defau;t
		 */

		createProtocolView();
	}

	public void updateAllViews() {
		updateAccountView();
		updateLayoutView();
		updateProtocolView(); /// not necessary without hotloadable protocols
	}

	public void createProtocolView() {

		protocolsButtonGroup = new ButtonGroup();
		protocolsListPanel = new VerticalScrollOnlyPanel();
		protocolsSettingsPanel = new VerticalScrollOnlyPanel();
		protocolView = new JSplitPane();

		protocolsListPanel.setLayout(new BoxLayout(protocolsListPanel, BoxLayout.Y_AXIS));
		protocolView.setLeftComponent(protocolsListPanel);
		protocolView.setRightComponent(protocolsSettingsPanel);
		updateProtocolView();

		this.setLayout(new BorderLayout());
		tabHandler.addTab("Layouts", layoutView);
		tabHandler.addTab("Accounts", accountView);
		tabHandler.addTab("Protocols", protocolView);
		tabHandler.addTab("General and About", PersistentData.editUserProfile());
		tabHandler.addTab("Simple Window", new JPanel());
		this.add(tabHandler, BorderLayout.CENTER);
	}

	ButtonGroup protocolsButtonGroup = null;
	VerticalScrollOnlyPanel protocolsListPanel = null;
	VerticalScrollOnlyPanel protocolsSettingsPanel = null;
	IProtocol selectedProtocol = null;

	private void updateProtocolView() {

		protocolsListPanel.removeAll();
		protocolsSettingsPanel.removeAll();

		final PersistentData profile = PersistentData.getUserProfile();

		final Dashboard updated = this;
		ArrayList<IProtocol> registered = profile.getRegisteredProtocols();
		for (IProtocol prot : registered) {
			final IProtocol f_protocol = prot;
			// System.out.println(prot.nickname());
			JToggleButton protButton = new JToggleButton("<html>" + prot.nickname() + "</html>");
			protocolsButtonGroup.add(protButton);

			if (f_protocol.equals(selectedProtocol)) {
				protButton.setSelected(true);
				Component c = f_protocol.protocolSettingsConfigurationMenu();
				protocolsSettingsPanel.add(c);
			}

			protButton.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					protocolsSettingsPanel.removeAll();
					Component c = f_protocol.protocolSettingsConfigurationMenu();
					protocolsSettingsPanel.add(c);
					selectedProtocol = f_protocol;
					updated.updateAllViews();
				}
			});

			protocolsListPanel.add(protButton);
		}

		if (selectedProtocol == null) {
			protocolsSettingsPanel.add(noProtocol);

		}
		redraw(protocolsListPanel);
		redraw(protocolView);
		redraw(protocolsSettingsPanel);
	}

	ButtonGroup accountsButtonGroup = null;
	VerticalScrollOnlyPanel accountListPanel = null;
	VerticalScrollOnlyPanel accountSettingsPanel = null;

	public void createAccountView() {
		JScrollPane accountSettingsScrollPane = null;
		JScrollPane accountListScrollPane = null;

		accountView = new JSplitPane();

		accountListPanel = new VerticalScrollOnlyPanel();
		accountListPanel.setBorder(BorderFactory.createEmptyBorder());
		accountListPanel.setLayout(new BoxLayout(accountListPanel, BoxLayout.Y_AXIS));

		accountSettingsPanel = new VerticalScrollOnlyPanel();
		accountSettingsPanel.setBorder(BorderFactory.createEmptyBorder());
		accountSettingsPanel.setLayout(new BorderLayout());
		accountSettingsPanel.add(noAccount, BorderLayout.CENTER);

		accountSettingsScrollPane = new JScrollPane(accountSettingsPanel);

		JPanel aboveAccountList = new JPanel();
		aboveAccountList.setLayout(new GridLayout(1, 2));
		JPanel leftPanel = new JPanel();
		leftPanel.setLayout(new BorderLayout());

		final IAccountRegisteredEventReciever arer = this; // for callbacks

		JButton addNewAccount = new JButton("Connect an Account");
		final JButton callerOfMenu = addNewAccount;
		addNewAccount.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				final PersistentData profile = PersistentData.getUserProfile();
				ArrayList<IProtocol> registeredProtocols = profile.getRegisteredProtocols();
				int length = registeredProtocols.size();
				IAction[] accountTypes = new IAction[length];
				for (int i = 0; i < length; i++) {
					final IProtocol fProtocol = registeredProtocols.get(i);
					accountTypes[i] = new IAction() {

						public String actionTitle() {
							return fProtocol.nickname();
						}

						public void performAction(IObjectCapabilities target) {
							fProtocol.authorizeNewAccount(arer);

						}

						public String helpInformation() {
							return fProtocol.protocolShortDescription();
						}

						public IIdentifier identifier() {
							// TODO Auto-generated method stub
							return null;
						}

					};
				}
				final IAction[] result = accountTypes;
				IObjectCapabilities oc = new IObjectCapabilities() {

					public IAction[] properties() {
						// TODO Auto-generated method stub
						return result;
					}
				};
				JPopupMenu menu = PopupMenus.createMenu(oc, "en_us");
				menu.show(callerOfMenu, callerOfMenu.getX(), callerOfMenu.getHeight());
			}

		});
		aboveAccountList.add(addNewAccount);

		leftPanel.add(aboveAccountList, BorderLayout.NORTH);

		accountListScrollPane = new JScrollPane(accountListPanel);

		leftPanel.add(accountListScrollPane, BorderLayout.CENTER);

		accountView.setLeftComponent(leftPanel);

		accountView.setRightComponent(accountSettingsScrollPane);
		updateAccountView();
	}

	public void updateAccountView() {
		accountsButtonGroup = new ButtonGroup();
		accountListPanel.removeAll();
		final PersistentData profile = PersistentData.getUserProfile();
		ArrayList<IAccount> registered = profile.getRegisteredAccounts();
		final Dashboard updated = this;
		for (IAccount acct : registered) {
			JToggleButton acctButton = new JToggleButton("<html>" + acct.nickname() + "</html>");
			accountsButtonGroup.add(acctButton);
			final IAccount accountInstance = acct;

			acctButton.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					accountSettingsPanel.removeAll();
					Component c = accountInstance.accountSettingsConfigurationMenu(updated);
					accountSettingsPanel.add(c, BorderLayout.CENTER);
					selected = accountInstance;

					JPanel topBarOfAccountView = new JPanel();
					JLabel titleOfAccountView = new JLabel(
							"<html>" + accountInstance.owningProtocol().protocolShortDescription() + "</html>");
					JButton forgetAccount = new JButton("forget this account");
					titleOfAccountView.setBorder(new EmptyBorder(new Insets(3, 5, 3, 5)));
					forgetAccount.addActionListener(new ActionListener() {

						public void actionPerformed(ActionEvent e) {
							profile.forgetAccount(accountInstance);
							accountSettingsPanel.removeAll();
							accountSettingsPanel.add(noAccount, BorderLayout.CENTER);
							updated.updateAllViews();
						}
					});
					topBarOfAccountView.setLayout(new BorderLayout());
					topBarOfAccountView.add(titleOfAccountView, BorderLayout.CENTER);
					topBarOfAccountView.add(forgetAccount, BorderLayout.EAST);
					accountSettingsPanel.add(topBarOfAccountView, BorderLayout.NORTH);
					// updated.updateAccountView();
				}
			});

			if (selected == acct) {
				acctButton.setSelected(true);
				redraw(acctButton);
			}
			accountListPanel.add(acctButton);
		}
		redraw(accountListPanel);
		redraw(accountView);
		redraw(accountSettingsPanel);
	}

	private void redraw(Component c) {
		c.repaint();
		c.revalidate();
	}

	ButtonGroup layoutsButtonGroup = null;

	// used for recursion in layoutCapList()
	public void addActionsFromLayoutNode(LayoutNode node, ArrayList<IAction> results) {
		if (node.children() == null)
			return;
		for (LayoutNode entry : node.children()) {
			final LayoutNode f_entry = entry;
			results.add(new IAction() {
				int depth = layersDeep;

				public void performAction(IObjectCapabilities target) {
					selectedLayoutNode = f_entry;
					updateLayoutView();
				}

				public String helpInformation() {
					// TODO Auto-generated method stub

					return "" + f_entry.identity().toString(); // not sure what else to put here, yet
				}

				public String actionTitle() {
					String pretext = "~ ";

					String title = "";
					if (f_entry.nodeTitle() == null)
						title = f_entry.identity().toString().substring(0, 6);
					else if (f_entry.nodeTitle().equals(""))
						title = f_entry.identity().toString().substring(0, 6);
					else
						title = f_entry.nodeTitle();

					return (String.join("", Collections.nCopies(depth, pretext)) + " " + title);
				}

				public IIdentifier identifier() {
					// TODO Auto-generated method stub
					return f_entry.identity();
				}
			});
			layersDeep++;
			addActionsFromLayoutNode(entry, results);
			layersDeep--;
		}
	}

	private int layersDeep = 0;

	public IObjectCapabilities layoutCapList() {
		layersDeep = 0;
		return new IObjectCapabilities() {

			public IAction[] properties() {
				// TODO Auto-generated method stub
				PersistentData profile = PersistentData.getUserProfile();
				LayoutNode registered = profile.displayLayouts();

				ArrayList<IAction> results = new ArrayList<IAction>();

				addActionsFromLayoutNode(registered, results);

				if (results.isEmpty()) {
					return null;
				} else {
					return results.toArray(new IAction[1]);
				}
			}
		};
	}

	GenericSelectionList layoutList = null;
	VerticalScrollOnlyPanel layoutEditPanel = null;

	LayoutNode selectedLayoutNode = null;

	JSplitPane layoutView = null;

	JTextArea noLayout = new JTextArea("<html>The layout editor will appear here when a layout is selected.</html>");

	JScrollPane layoutListScrollPane = null;

	JScrollPane layoutEditScrollPane = null;

	JButton moveUpward, moveDownward, moveDeeper, moveOut;

	Component currentLayoutNode = null;

	public void createLayoutView() {

		layoutView = new JSplitPane();

		JPanel aboveLayoutList = new JPanel();
		aboveLayoutList.setLayout(new GridLayout(1, 2));

		JPanel aboveLayoutEditor = new JPanel();
		aboveLayoutEditor.setLayout(new GridLayout(1, 4));

		moveUpward = new JButton("Shift Up");
		moveDownward = new JButton("Shift Down");
		moveOut = new JButton("Shift Out");
		moveDeeper = new JButton("Shift Into");

		aboveLayoutEditor.add(moveUpward);
		aboveLayoutEditor.add(moveDownward);
		aboveLayoutEditor.add(moveOut);
		aboveLayoutEditor.add(moveDeeper);

		moveUpward.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (selectedLayoutNode != null) {
					PersistentData profile = PersistentData.getUserProfile();
					LayoutNode registered = profile.displayLayouts();
					selectedLayoutNode.moveUp(registered);
					updateLayoutView();
				}
			}
		});

		moveDownward.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (selectedLayoutNode != null) {
					PersistentData profile = PersistentData.getUserProfile();
					LayoutNode registered = profile.displayLayouts();
					selectedLayoutNode.moveDown(registered);
					updateLayoutView();
				}
			}
		});

		moveOut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (selectedLayoutNode != null) {
					PersistentData profile = PersistentData.getUserProfile();
					LayoutNode registered = profile.displayLayouts();
					selectedLayoutNode.expand(registered);
					updateLayoutView();
				}
			}
		});

		moveDeeper.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (selectedLayoutNode != null) {
					PersistentData profile = PersistentData.getUserProfile();
					LayoutNode registered = profile.displayLayouts();
					selectedLayoutNode.shrink(registered);
					updateLayoutView();
				}
			}
		});

		JPanel leftPanel = new JPanel();
		leftPanel.setLayout(new BorderLayout());

		JPanel rightPanel = new JPanel();
		rightPanel.setLayout(new BorderLayout());

		JButton addNewLayout = new JButton("Create A New Layout");

		addNewLayout.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				PersistentData profile = PersistentData.getUserProfile();
				LayoutNode registered = profile.displayLayouts();
				if (selectedLayoutNode != null) {
					// selectedLayoutNode.findParent(registered).children().add(LayoutNode.createGeneric());
					selectedLayoutNode.addAbove(LayoutNode.createGeneric(), registered);
					System.out.println("attempting to add above " + selectedLayoutNode.identity().toString());
					updateLayoutView();
				} else {

					registered.children().add(LayoutNode.createGeneric());
					System.out.println("defaulting to adding to end of list");
					updateLayoutView();
				}
			}

		});
		aboveLayoutList.add(addNewLayout);

		leftPanel.add(aboveLayoutList, BorderLayout.NORTH);
		leftPanel.setBorder(BorderFactory.createEmptyBorder());

		IObjectCapabilities oc = layoutCapList();

		layoutList = new GenericSelectionList(oc);
		layoutList.setBorder(BorderFactory.createMatteBorder(1, 3, 1, 1, Color.DARK_GRAY));

		layoutListScrollPane = new JScrollPane(layoutList);

		leftPanel.add(layoutListScrollPane, BorderLayout.CENTER);

		layoutView.setLeftComponent(leftPanel);

		layoutEditPanel = new VerticalScrollOnlyPanel();
		layoutEditPanel.setBorder(BorderFactory.createEmptyBorder());
		layoutEditPanel.setLayout(new BorderLayout());
		layoutEditPanel.add(noLayout, BorderLayout.CENTER);

		layoutEditScrollPane = new JScrollPane(layoutEditPanel);
		rightPanel.add(layoutEditScrollPane, BorderLayout.CENTER);
		rightPanel.add(aboveLayoutEditor, BorderLayout.NORTH);

		layoutView.setRightComponent(rightPanel);

		updateLayoutView();
	}

	public void updateLayoutView() {

		layoutsButtonGroup = new ButtonGroup();

		PersistentData profile = PersistentData.getUserProfile();
		final LayoutNode layout = selectedLayoutNode;
		final LayoutNode root = profile.displayLayouts();

		layoutEditPanel.removeAll();
		if (layout != null) {
			if (layout.hasBefore(root)) {
				moveUpward.setEnabled(true);
				moveDeeper.setEnabled(true);
			} else {
				moveUpward.setEnabled(false);
				moveDeeper.setEnabled(false);
			}
			if (layout.findParent(root) != root) {
				moveOut.setEnabled(true);
			} else {
				moveOut.setEnabled(false);
			}
			if (layout.hasAfter(root)) {
				moveDownward.setEnabled(true);
			} else {
				moveDownward.setEnabled(false);
			}
			currentLayoutNode = layout.nodeEditor(this);
			layoutEditPanel.add(currentLayoutNode, BorderLayout.CENTER);
		} else {
			moveUpward.setEnabled(false);
			moveDownward.setEnabled(false);
			moveOut.setEnabled(false);
			moveDeeper.setEnabled(false);
			layoutEditPanel.add(noLayout, BorderLayout.CENTER);
		}
		layoutList.wrapped = layoutCapList();
		layoutList.updateComponents();

		redraw(layoutList);
		redraw(layoutEditPanel);
	}

	public void accountRegistered(IAccount acc) {
		PersistentData profile = PersistentData.getUserProfile();
		profile.accountRegistered(acc);
		updateAccountView();
	}

	public void receiveEvent(Object event) {
		// hacky method to make sure to update after event finishes (specifically
		// resizing)
		// TODO: is there a better alternative?
		// updateAllViews();

		new Thread() {
			public void run() {
				try {
					sleep(20);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				updateAllViews();
			}
		}.start();
	}
}
