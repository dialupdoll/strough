package io.gitlab.dahlterm.strough.app.ui;

import io.gitlab.dahlterm.strough.app.Utilities;
import io.gitlab.dahlterm.strough.app.event.IEventReciever;
import io.gitlab.dahlterm.strough.app.protocol.IChannel;
import io.gitlab.dahlterm.strough.app.protocol.IContentPage;
import io.gitlab.dahlterm.strough.app.protocol.IMessage;
import io.gitlab.dahlterm.strough.app.plain_text.DefaultComposeComponent;
import io.gitlab.dahlterm.strough.app.plain_text.DefaultMessageComponent;
import io.gitlab.dahlterm.strough.app.ui.PopupMenus;
import io.gitlab.dahlterm.strough.app.ui.VerticalScrollOnlyPanel;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

class FeedPanelWithinScroller extends JPanel implements IEventReciever {

VerticalScrollOnlyPanel scrollHandler = new VerticalScrollOnlyPanel(); // scroll pane why are you like this
JScrollPane scrollPane = new JScrollPane(scrollHandler);
DefaultComposeComponent composeMessage;

IChannel target = null;

IContentPage page = null;

JLabel feedName = new JLabel(), feedTopic = new JLabel();
JButton feedMenu = new JButton("...");

ArrayList<Component> messageComponents = new ArrayList<Component>();

public FeedPanelWithinScroller(IChannel targetChan) {

	setMinimumSize(new Dimension(300, 250));

	scrollHandler.setLayout(new BoxLayout(scrollHandler, BoxLayout.Y_AXIS));
	scrollHandler.setAlignmentX(Component.LEFT_ALIGNMENT);
	scrollHandler.setAlignmentY(Component.BOTTOM_ALIGNMENT);
	scrollHandler.setBorder(BorderFactory.createEmptyBorder());

	scrollPane.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));

	target = targetChan;

	targetChan.setUpdateReciever(this);

	JPanel topBar = new JPanel();
	topBar.setLayout(new BorderLayout());
	topBar.add(feedTopic, BorderLayout.CENTER);
	topBar.add(feedName, BorderLayout.WEST);
	topBar.add(feedMenu, BorderLayout.EAST);
	feedName.setBorder(new EmptyBorder(0, 4, 0, 4));
	feedTopic.setBorder(new EmptyBorder(0, 4, 0, 4));

	scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
	setLayout(new BorderLayout());
	add(scrollPane, BorderLayout.CENTER);
	if (target != null) {
		composeMessage = new DefaultComposeComponent(target);
	}
	add(composeMessage, BorderLayout.SOUTH);
	add(topBar, BorderLayout.NORTH);

	feedMenu.addActionListener(new ActionListener() {

		public void actionPerformed(ActionEvent e) {
			if (target.properties() != null) {
				JPopupMenu menu = PopupMenus.createMenu(target, "en-us");
				menu.show(feedMenu, 0, feedMenu.getHeight());
			}
		}

	});

	updateChat();

}

public void addMessages(IContentPage contents) {
	IMessage[] messages = contents.contents();

	if (messages == null)
		return;
	for (IMessage msg : messages) {
		DefaultMessageComponent mc = new DefaultMessageComponent(msg);
		msg.setUpdateReciever(this);

		mc.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		scrollHandler.add(mc);
		messageComponents.add((Component) mc);
	}
}

public boolean isScrolledBackward() {
	return false;
}

public void scrollToPresent() {
	if (scrollPane == null)
		return;
	JScrollBar vertical = scrollPane.getVerticalScrollBar();
	vertical.setValue(vertical.getMaximum());
}

public void updateChat() {

	feedName.setText(target.title());
	feedName.setToolTipText(target.title());
	Utilities.redraw(feedName);

	feedTopic.setText(target.topic());
	feedTopic.setToolTipText(target.topic());
	Utilities.redraw(feedTopic);

	Font f = feedTopic.getFont();
	feedTopic.setFont(f.deriveFont(f.getStyle() & ~Font.BOLD));

	page = target.retrievePage(null);
	target.setUpdateReciever(this);
	scrollHandler.removeAll();
	addMessages(page);
	scrollPane.repaint();
	scrollPane.revalidate();
	for (Component c : messageComponents) {
		c.repaint();
		c.revalidate();
	}
	Utilities.redraw(composeMessage);

	// hacky method to make sure to update the scroll pane after it is finished.
	// not sure how else to do it yet.
	Thread thread = new Thread() {
		public void run() {
			try {
				sleep(20);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if (!isScrolledBackward()) {
				scrollToPresent();
				for (Component c : messageComponents) {
					Utilities.redraw(c);
				}
			}
		}
	};

	thread.start();

}

public void receiveEvent(Object event) {
	updateChat();
}
}
