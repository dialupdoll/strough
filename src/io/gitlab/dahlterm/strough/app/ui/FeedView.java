package io.gitlab.dahlterm.strough.app.ui;


import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
//import java.util.ArrayList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.UUID;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;

import io.gitlab.dahlterm.strough.app.*;
import io.gitlab.dahlterm.strough.app.protocol.*;
import io.gitlab.dahlterm.strough.app.plain_text.DefaultComposeComponent;

public class FeedView extends JPanel {

	public LayoutNode config = null;

	JSplitPane splitter = new JSplitPane();
	GenericSelectionList leftSide = null;
	VerticalScrollOnlyPanel scrollHandler = new VerticalScrollOnlyPanel(); // scroll pane why are you like this
	JScrollPane scrollPane = new JScrollPane(scrollHandler);
	DefaultComposeComponent composeMessage;
	JPanel rightSide = new JPanel();

	IChannel target = null;

	IContentPage page = null;
	
	JLabel feedName = new JLabel(), feedTopic = new JLabel();
	
	JLabel layoutEmptyChannel = new JLabel("No channel endpoint to display! Check your layout editor in the dashboard., if this is in error.");
	JLabel layoutEmptyChildren = new JLabel("No child channels to display! You probably set the wrong layout node to open, or opened the wrong layout node manually. Check your layout editor in the dashboard, if this is in error.");
	JButton feedMenu = new JButton("...");
	
	ArrayList<Component> messageComponents = new ArrayList<Component>();

	public FeedView(LayoutNode configuration) {
		config = configuration;
		updateLayoutView();
		splitter.setLeftComponent(leftSide);
		if(configuration.children() == null | configuration.children().isEmpty()) {
			splitter.setLeftComponent(layoutEmptyChildren);
		}
		splitter.setRightComponent(rightSide);
		setMinimumSize(new Dimension(300, 250));
		setLayout(new GridLayout(1,1));	/// this makes it scale right and fill the space.
		add(splitter);
	}
	FeedPanelWithinScroller fp = null;
	public void setFocus(LayoutNode focus) {
		
		target = focus.endpoint();
		
		if(target == null) {

			rightSide.add(layoutEmptyChannel);
			return;
		} else {
			fp = new FeedPanelWithinScroller(target);
			rightSide.add(fp);
			target.setUpdateReciever(fp);
		}
		
		
		splitter.setRightComponent(rightSide);
		
	}


	LayoutNode selectedLayoutNode = null;

	public void updateLayoutView() {
		if(leftSide == null) {
			leftSide = new GenericSelectionList(layoutCapList());
		}
	
		rightSide.removeAll();
		if(selectedLayoutNode != null) {
			setFocus(selectedLayoutNode);
		} else {
			rightSide.setLayout(new GridLayout(1, 1));
			rightSide.add(layoutEmptyChannel);
		}
		leftSide.wrapped = layoutCapList();
		leftSide.updateComponents();
		
		Utilities.redraw(leftSide);
		Utilities.redraw(rightSide);
	}

	
	public void addActionsFromLayoutNode(LayoutNode node, ArrayList<IAction> results) {
		if(node.children() == null) return;
		for(LayoutNode entry : node.children()) {
			final LayoutNode f_entry = entry;
			results.add(new IAction() {
				int depth = layersDeep;
			public void performAction(IObjectCapabilities target) {
					selectedLayoutNode = f_entry;
					updateLayoutView();
				}
				
				public String helpInformation() {
					return "" + f_entry.identity().toString();		//not sure what else to put here, yet
				}
				
				public String actionTitle() {
					String pretext = "~ ";
					
					String title = "";
					if(f_entry.nodeTitle() == null)
						title = f_entry.identity().toString().substring(0, 6);
					else if(f_entry.nodeTitle().equals(""))
						title = f_entry.identity().toString().substring(0, 6);
					else
						title = f_entry.nodeTitle();
					
					return (String.join("", Collections.nCopies(depth, pretext)) + " " + title);
				}

				public IIdentifier identifier() {
					return f_entry.identity();
				}
			});
			layersDeep++;
			addActionsFromLayoutNode(entry, results);
			layersDeep--;
		}
	}
	private int layersDeep = 0;
	public IObjectCapabilities layoutCapList() {
		layersDeep = 0;
		return new IObjectCapabilities() {
			
			public IAction[] properties() {
				LayoutNode registered = config;
				
				ArrayList<IAction> results = new ArrayList<IAction>();
				
				addActionsFromLayoutNode(registered, results);
				
				if(results.isEmpty()) {
					System.out.println("no channels or layouts to display!");
					return null;
				} else {
					return results.toArray(new IAction[1]);
				}
			}
		};
	}


}
