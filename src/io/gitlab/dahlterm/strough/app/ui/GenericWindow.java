package io.gitlab.dahlterm.strough.app.ui;

import io.gitlab.dahlterm.strough.app.Resources;

import java.awt.Dimension;
import java.awt.Image;

import javax.swing.JComponent;
import javax.swing.JFrame;

public class GenericWindow extends JFrame {

	public GenericWindow(String title, JComponent component, boolean exitOnClose) {
		super(title);
		if(exitOnClose)
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Image i = Resources.loadResource("/sio.svg-32.png");
		if(i != null)
			setIconImage(i);
		this.setContentPane(component);
		setMinimumSize(component.getMinimumSize());
		pack();
		setSize(new Dimension(750, 550));
	}
}
