package io.gitlab.dahlterm.strough.app.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import io.gitlab.dahlterm.strough.app.protocol.IAction;
import io.gitlab.dahlterm.strough.app.protocol.IObjectCapabilities;

public class PopupMenus {
	public static JPopupMenu createMenu(IObjectCapabilities target, String localization) {
		// final versions let us access these in anon classes
		final IObjectCapabilities ocapTarget = target;
		final JPopupMenu result = new JPopupMenu();
		
		IAction[] elements = target.properties();
		for(IAction item : elements) {
			final IAction finalItem = item;
			JMenuItem menuItem = new JMenuItem(item.actionTitle());
			menuItem.setToolTipText(item.helpInformation());
			menuItem.addActionListener(new ActionListener() {
				IAction menuAction = finalItem;
				IObjectCapabilities capabilitiesTarget = ocapTarget;
				public void actionPerformed(ActionEvent e) {
					menuAction.performAction(capabilitiesTarget);
				}
				
			});
			result.add(menuItem);
		}
		return result;
	}
}
