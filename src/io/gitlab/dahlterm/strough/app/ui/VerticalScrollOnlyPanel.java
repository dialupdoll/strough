package io.gitlab.dahlterm.strough.app.ui;

import java.awt.Dimension;
import java.awt.Rectangle;

import javax.swing.JPanel;
import javax.swing.Scrollable;

/**
 * Turns out JPanel doesn't implement Scrollable, which lets you control layout managers in JScrollPanes better.
 * This helper class does that to make our code less bulky elsewhere.
 */
public class VerticalScrollOnlyPanel extends JPanel implements Scrollable {
	public int scrollUnitIncrement = 10, scrollBlockIncrement = 10;

	public int getScrollableUnitIncrement(Rectangle visibleRect, int orientation, int direction) {
		// TODO make it configurable per-account or globally in the client
		return scrollUnitIncrement;
	}

	public int getScrollableBlockIncrement(Rectangle visibleRect, int orientation, int direction) {
		// TODO make it configurable per-account or globally in the client
		return scrollBlockIncrement;
	}
	
	// below: This makes it not scroll horizontally, very important

	public boolean getScrollableTracksViewportWidth() {
		return true;
	}

	public boolean getScrollableTracksViewportHeight() {
		return false;
	}

	public Dimension getPreferredScrollableViewportSize() {
		return null;
	}

}
